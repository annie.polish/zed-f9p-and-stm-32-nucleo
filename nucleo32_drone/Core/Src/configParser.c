/*
 * configurator.c
 *
 *  Created on: Aug 26, 2020
 *      Author: annie
 */

#include <configParser.h>
#include "log.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// has a config file been parsed yet?
bool cfgDone = false;

// maximum line length and line count for the config file
int lineLen = 32;
int numLines = 10;

// default values of config vars
bool cfgSurvey = true; // is SURVEY set to true or false?
int32_t cfgLat = 0; // fixed latitude, formatted to be ready to send
int32_t cfgLon = 0;
int32_t cfgHeight = 0;
uint32_t cfgTime = 60;
uint32_t cfgAcc = 500000;
uint16_t cfgRate = 1000;
uint32_t cfgTPPeriod = 1000000;
uint32_t cfgTPFreq = 1;
uint32_t cfgTPLen = 500000;
double cfgTPDuty = 50.0;
bool cfgTelem = true; // will the rover send telemetry packets back?

// tracks whether each config var has been read
bool readSurvey = false;
bool readLat = false;
bool readLon = false;
bool readHeight = false;
bool readTime = false;
bool readAcc = false;
bool readRate = false;
bool readTPPeriod = false;
bool readTPFreq = false;
bool readTPLen = false;
bool readTPDuty = false;
bool readTelem = false;

UART_HandleTypeDef huart2;
DMA_HandleTypeDef hdma_usart2_tx;

void parseLine(char * line) {
	int eqIdx = strchr(line, '=') - line;

	if (strncmp("SURVEY", line, 6) == 0 && !readSurvey) {
		if (line[eqIdx+1] == 'T' || line[eqIdx+1] == 't' || line[eqIdx+1] == '1') {
			cfgSurvey = true;
			readSurvey = true;
		} else if (line[eqIdx+1] == 'F' || line[eqIdx+1] == 'f' || line[eqIdx+1] == '0') {
			cfgSurvey = false;
			readSurvey = true;
		}
	} else if (strncmp("FIXLAT", line, 6) == 0 && !readLat) {
		bool isNegative = false;
		bool reachedDec = false;
		int multiplier = 1000000;
		cfgLat = 0;
		int idx = eqIdx+1;
		if (line[idx] == '-') { isNegative = true; idx++; }
		while(isdigit(line[idx]) || line[idx] == '.') {
			// at the decimal point, shift the number up and flip a flag
			if (line[idx] == '.') {
				reachedDec = true;
				cfgLat = cfgLat * 10000000;
				// before the decimal, keep multiplying the accumulator and adding to it
			} else if (!reachedDec) {
				cfgLat = cfgLat * 10 + (line[idx] - 0x30);
				// after the decimal, add digits times a decreasing multiplier
			} else {
				cfgLat = cfgLat + (multiplier * (line[idx]-0x30) );
				if (multiplier == 1) multiplier = 0;
				multiplier = multiplier/10;
			}
			idx++;
		}
		if (!reachedDec) cfgLat *= 10000000;
		if (isNegative) cfgLat = (-1)*cfgLat;
		readLat = true;

	} else if (strncmp("FIXLON", line, 6) == 0 && !readLon) {
		bool isNegative = false;
		bool reachedDec = false;
		int multiplier = 1000000;
		cfgLon = 0;
		int idx = eqIdx+1;
		if (line[idx] == '-') { isNegative = true; idx++; }
		while(isdigit(line[idx]) || line[idx] == '.') {
			// at the decimal point, shift the number up and flip a flag
			if (line[idx] == '.') {
				reachedDec = true;
				cfgLon = cfgLon * 10000000;
				// before the decimal, keep multiplying the accumulator and adding to it
			} else if (!reachedDec) {
				cfgLon = cfgLon * 10 + (line[idx] - 0x30);
				// after the decimal, add digits times a decreasing multiplier
			} else {
				cfgLon = cfgLon + (multiplier * (line[idx]-0x30) );
				if (multiplier == 1) multiplier = 0;
				multiplier = multiplier/10;
			}
			idx++;
		}
		if (!reachedDec) cfgLon *= 10000000;
		if (isNegative) cfgLon = (-1)*cfgLon;
		readLon = true;

	} else if (strncmp("FIXHEIGHT", line, 6) == 0 && !readHeight) {
		bool isNegative = false;
		bool reachedDec = false;
		int multiplier = 10;
		cfgHeight = 0;
		int idx = eqIdx+1;
		if (line[idx] == '-') { isNegative = true; idx++; }
		while(isdigit(line[idx]) || line[idx] == '.') {
			// at the decimal point, shift the number up and flip a flag
			if (line[idx] == '.') {
				reachedDec = true;
				cfgHeight = cfgHeight * 100;
				// before the decimal, keep multiplying the accumulator and adding to it
			} else if (!reachedDec) {
				cfgHeight = cfgHeight * 10 + (line[idx] - 0x30);
				// after the decimal, add digits times a decreasing multiplier
			} else {
				cfgHeight = cfgHeight + (multiplier * (line[idx]-0x30) );
				if (multiplier == 1) multiplier = 0;
				multiplier = multiplier/10;
			}
			idx++;
		}

		if (!reachedDec) cfgHeight*= 100;
		if (isNegative) cfgHeight = (-1)*cfgHeight;
		readHeight = true;

	} else if (strncmp("SVTIME", line, 6) == 0 && !readTime) {
		cfgTime = 0;
		int idx = eqIdx+1;
		while(isdigit(line[idx])) {
			cfgTime = cfgTime * 10 + (line[idx] - 0x30);
			idx++;
		}
		readTime = true;
	} else if (strncmp("SVACC", line, 5) == 0 && !readAcc) {
		cfgAcc = 0;
		int idx = eqIdx+1;
		while(isdigit(line[idx])) {
			cfgAcc = cfgAcc * 10 + (line[idx] - 0x30);
			idx++;
		}

		cfgAcc *= 100;
		readAcc = true;
	} else if (strncmp("GPSRATE", line, 7) == 0 && !readRate) {
		cfgRate = 0;
		int idx = eqIdx+1;
		while(isdigit(line[idx])) {
			cfgRate = cfgRate * 10 + (line[idx] - 0x30);
			idx++;
		}
		readRate = true;
	} else if (strncmp("TPPER", line, 5) == 0 && !readTPPeriod && !readTPFreq) {
		cfgTPPeriod = 0;
		int idx = eqIdx+1;
		while(isdigit(line[idx])) {
			cfgTPPeriod = cfgTPPeriod * 10 + (line[idx] - 0x30);
			idx++;\
		}
		readTPPeriod = true;
	} else if (strncmp("TPLEN", line, 5) == 0 && !readTPLen && !readTPDuty) {
		cfgTPLen = 0;
		int idx = eqIdx+1;
		while(isdigit(line[idx])) {
			cfgTPLen = cfgTPLen * 10 + (line[idx] - 0x30);
			idx++;
		}
		readTPLen = true;
	} else if (strncmp("TELEM", line, 5) == 0 && !readTelem) {
		if (line[eqIdx+1] == 'T' || line[eqIdx+1] == 't' || line[eqIdx+1] == '1') {
			cfgTelem = true;
			readTelem = true;
		} else if (line[eqIdx+1] == 'F' || line[eqIdx+1] == 'f' || line[eqIdx+1] == '0') {
			cfgTelem = false;
			readTelem = true;
		}
	}

}


void openLogParseConfig() {
	// allocate an 81 character buffer, for an 80 char line plus a newline
	char buffer[81] = "                                                                                "; //im so sorry.
	uint8_t idx = 0; // index into the buffer
	char locbuf[6] = "     ";
	uint32_t floc = 0; // index into the file
	buffer[0] = 0; // make sure round 1 of my loop runs...

	// put the openlog into command mode
	uint8_t esc = 26;
	HAL_UART_Transmit(&huart2, &esc, 1, 100);
	HAL_Delay(10);
	HAL_UART_Transmit(&huart2, &esc, 1, 100);
	HAL_Delay(10);
	HAL_UART_Transmit(&huart2, &esc, 1, 100);
	HAL_Delay(100);
    HAL_UART_Receive(&huart2, &(buffer[idx]), 1, 100);


	// start reading from the file
	char readStr[17] = "read gpscfg.txt ";
	char numStr[3] = " 1";
	char retStr[3] = "\r\n";
	HAL_UART_Transmit(&huart2, retStr, 2, 100);
    HAL_UART_Receive(&huart2, &(buffer[idx]), 1, 100);
    HAL_UART_Receive(&huart2, &(buffer[idx]), 1, 100);
    HAL_UART_Receive(&huart2, &(buffer[idx]), 1, 100);

	bool breaknow = false;
	HAL_StatusTypeDef rxStatus;
	while (!breaknow && idx <= 80) {
		// ask the SD card for another character
		//locbuf = "\0\0\0\0\0\0";
		itoa(floc, locbuf, 10); floc++;
		//sprintf(locbuf, " %u ", floc); floc++;
		HAL_UART_Transmit(&huart2, readStr, 16, 100);
		HAL_UART_Transmit(&huart2, locbuf, strlen(locbuf), 100);
		HAL_UART_Transmit(&huart2, numStr, 2, 100);
		//HAL_UART_Receive_IT(&huart2, &(buffer[idx]), 5);
		HAL_UART_Transmit(&huart2, retStr, 1, 100);


		// catch the character it sends
		//rxStatus = HAL_UART_Receive(&huart2, &(buffer[idx]), 1, 100);
		rxStatus = HAL_UART_Receive(&huart2, &(buffer[idx]), 1, 100);
		rxStatus = HAL_UART_Receive(&huart2, &(buffer[idx]), 1, 100);
		rxStatus = HAL_UART_Receive(&huart2, &(buffer[idx]), 1, 100);
        rxStatus = HAL_UART_Receive(&huart2, &(buffer[idx+1]), 1, 100);

//        rxStatus = HAL_UART_Receive(&huart2, &(buffer[idx]), 1, 100);
//        rxStatus = HAL_UART_Receive(&huart2, &(buffer[idx+1]), 1, 100);
//        rxStatus = HAL_UART_Receive(&huart2, &(buffer[idx+2]), 1, 100);
//        rxStatus = HAL_UART_Receive(&huart2, &(buffer[idx+3]), 1, 100);
//        rxStatus = HAL_UART_Receive(&huart2, &(buffer[idx+4]), 1, 100);

		if (buffer[idx-1] == '!' || buffer[idx] == '!' || rxStatus != HAL_OK) {
			breaknow = true;
		}

		// try to receive an extra character. if it works, that means we're seeing an error message
        rxStatus = HAL_UART_Receive(&huart2, &(buffer[idx+2]), 1, 100);
        rxStatus = HAL_UART_Receive(&huart2, &(buffer[idx+2]), 1, 100);

		if (HAL_UART_Receive(&huart2, &(buffer[idx+2]), 1, 10) == HAL_OK) {
		    breaknow = true;
		}

		idx++;

		// check for newlines
		if (buffer[idx-1] == '\n') {
			// apply the config option
			parseLine(buffer);
			// reset the index
			idx = 0;
		}

	}
	// set the "read config" flag
	cfgDone = true;

}

/*
// parses a config file from the SD card
bool parseConfig() {
	char * buffer = malloc(lineLen * numLines);
	bool retVal = false;
	bool breakNow = false;
	char errorString[] = "Error on line    \n";
	if (logNameSet) {
		Mount_SD("/");
		if (Read_File("/CFG.TXT", buffer, lineLen * numLines)==FR_OK) {
			writeLogLine("Parsing CFG.TXT\n");

			// discard the first line
			char * line = buffer;
			int lineNum = 1;

			// parse through all the subsequent lines
			while (!breakNow) {
				// locate the equals sign
				char * eqPtr = strchr(line, '=');
				int eqIdx = 0;
				if (eqPtr == NULL) { // if there's no equals sign, this was an invalid line
					if (line[0] != 0) {
						sprintf(errorString+14, "%d", lineNum);
						writeLogLine(errorString);
					}
					// find the next line
					line = strchr(line, '\n');
					if (line == NULL) { breakNow = true; }
					else { line += 1; lineNum++; }
					continue;
				} else { // convert the eqLoc from pointer to index
					eqIdx = eqPtr - line;
				}

				// check the key against a list of possible keys
				// todo split up on key length

				// set the survey-in mode
				if (strncmp("SURVEY", line, 6) == 0 && !readSurvey) {
					if (line[eqIdx+1] == 'T' || line[eqIdx+1] == 't' || line[eqIdx+1] == '1') {
						cfgSurvey = true;
						readSurvey = true;
						writeLogLine("Survey mode set\n");
					} else if (line[eqIdx+1] == 'F' || line[eqIdx+1] == 'f' || line[eqIdx+1] == '0') {
						cfgSurvey = false;
						readSurvey = true;
						writeLogLine("Fixed location mode set\n");
					} else {
						writeLogLine("Error: SURVEY must be TRUE or FALSE\n");
					}

				} else if (strncmp("FIXLAT", line, 6) == 0 && !readLat) {
					bool isNegative = false;
					bool reachedDec = false;
					int multiplier = 1000000;
					cfgLat = 0;
					int idx = eqIdx+1;
					if (line[idx] == '-') { isNegative = true; idx++; }
					while(isdigit(line[idx]) || line[idx] == '.') {
						// at the decimal point, shift the number up and flip a flag
						if (line[idx] == '.') {
							reachedDec = true;
							cfgLat = cfgLat * 10000000;
							// before the decimal, keep multiplying the accumulator and adding to it
						} else if (!reachedDec) {
							cfgLat = cfgLat * 10 + (line[idx] - 0x30);
							// after the decimal, add digits times a decreasing multiplier
						} else {
							cfgLat = cfgLat + (multiplier * (line[idx]-0x30) );
							if (multiplier == 1) multiplier = 0;
							multiplier = multiplier/10;
						}
						idx++;
					}
					if (!reachedDec) cfgLat *= 10000000;
					if (isNegative) cfgLat = (-1)*cfgLat;
					char logString[] = "Latitude:                \n";
					strncpy(logString+10, line+eqIdx+1, idx-(eqIdx+1));
					writeLogLine(logString);
					readLat = true;

				} else if (strncmp("FIXLON", line, 6) == 0 && !readLon) {
					bool isNegative = false;
					bool reachedDec = false;
					int multiplier = 1000000;
					cfgLon = 0;
					int idx = eqIdx+1;
					if (line[idx] == '-') { isNegative = true; idx++; }
					while(isdigit(line[idx]) || line[idx] == '.') {
						// at the decimal point, shift the number up and flip a flag
						if (line[idx] == '.') {
							reachedDec = true;
							cfgLon = cfgLon * 10000000;
							// before the decimal, keep multiplying the accumulator and adding to it
						} else if (!reachedDec) {
							cfgLon = cfgLon * 10 + (line[idx] - 0x30);
							// after the decimal, add digits times a decreasing multiplier
						} else {
							cfgLon = cfgLon + (multiplier * (line[idx]-0x30) );
							if (multiplier == 1) multiplier = 0;
							multiplier = multiplier/10;
						}
						idx++;
					}
					if (!reachedDec) cfgLon *= 10000000;
					if (isNegative) cfgLon = (-1)*cfgLon;
					char logString[] = "Longitude:                \n";
					strncpy(logString+11, line+eqIdx+1, idx-(eqIdx+1));
					writeLogLine(logString);
					readLon = true;

				} else if (strncmp("FIXHEIGHT", line, 6) == 0 && !readHeight) {
					bool isNegative = false;
					bool reachedDec = false;
					int multiplier = 10;
					cfgHeight = 0;
					int idx = eqIdx+1;
					if (line[idx] == '-') { isNegative = true; idx++; }
					while(isdigit(line[idx]) || line[idx] == '.') {
						// at the decimal point, shift the number up and flip a flag
						if (line[idx] == '.') {
							reachedDec = true;
							cfgHeight = cfgHeight * 100;
							// before the decimal, keep multiplying the accumulator and adding to it
						} else if (!reachedDec) {
							cfgHeight = cfgHeight * 10 + (line[idx] - 0x30);
							// after the decimal, add digits times a decreasing multiplier
						} else {
							cfgHeight = cfgHeight + (multiplier * (line[idx]-0x30) );
							if (multiplier == 1) multiplier = 0;
							multiplier = multiplier/10;
						}
						idx++;
					}

					if (!reachedDec) cfgHeight*= 100;
					if (isNegative) cfgHeight = (-1)*cfgHeight;
					char logString[] = "Height:                \n";
					strncpy(logString+8, line+eqIdx+1, idx-(eqIdx+1));
					writeLogLine(logString);
					readHeight = true;

				} else if (strncmp("SVTIME", line, 6) == 0 && !readTime) {
					cfgTime = 0;
					int idx = eqIdx+1;
					while(isdigit(line[idx])) {
						cfgTime = cfgTime * 10 + (line[idx] - 0x30);
						idx++;
					}

					char logString[] = "Survey time:                \n";
					strncpy(logString+12, line+eqIdx+1, idx-(eqIdx+1));
					writeLogLine(logString);
					readTime = true;
				} else if (strncmp("SVACC", line, 5) == 0 && !readAcc) {
					cfgAcc = 0;
					int idx = eqIdx+1;
					while(isdigit(line[idx])) {
						cfgAcc = cfgAcc * 10 + (line[idx] - 0x30);
						idx++;
					}

					cfgAcc *= 100;
					char logString[] = "Survey accuracy:                \n";
					strncpy(logString+16, line+eqIdx+1, idx-(eqIdx+1));
					writeLogLine(logString);
					readAcc = true;
				} else if (strncmp("GPSRATE", line, 7) == 0 && !readRate) {
					cfgRate = 0;
					int idx = eqIdx+1;
					while(isdigit(line[idx])) {
						cfgRate = cfgRate * 10 + (line[idx] - 0x30);
						idx++;
					}

					char logString[] = "GPS integration period (ms):         \n";
					strncpy(logString+28, line+eqIdx+1, idx-(eqIdx+1));
					writeLogLine(logString);
					readRate = true;
				}
				// find the next line
				line = strchr(line, '\n');
				if (line == NULL) { breakNow = true; }
				else { line += 1; lineNum++; }
			}

			Unmount_SD("/");
			configDone = true;

			// check that the config information is fully specified
			if (cfgSurvey == true) {
				if (!readTime) {
					writeLogLine("WARNING: Survey length not specified (using 60s by default)\n");
				}
				if (!readAcc) {
					writeLogLine("WARNING: Survey accuracy not specified (using 50m by default)\n");
				}
				if (cfgTime < 10) {
					writeLogLine("WARNING: Very short survey time\n");
				}
				if (cfgAcc < 10000) {
					writeLogLine("WARNING: Sub-meter survey accuracy requested. This will take a while.\n");
				}
				retVal = true;
			} else {
				if (!readLat || !readLon || !readHeight) {
					writeLogLine("ERROR: Fixed location not fully specified\n");
					retVal = false;
				} else {
					retVal = true;
				}
			}
		} else {
			writeLogLine("Error: CFG.TXT not found\n");
			Unmount_SD("/");
			retVal =  false;
		}
	} else {
		retVal = false;
	}
	free(buffer);
	return retVal;
}
*/
