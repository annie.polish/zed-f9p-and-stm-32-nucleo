/*
 * neopix.c
 *
 *  Created on: Mar 8, 2022
 *      Author: annie
 */


#include "neopix.h"
#include <stdbool.h>
#include <string.h>

// defines the SPI interface for the neopixels
SPI_HandleTypeDef hspi1;
DMA_HandleTypeDef hdma_spi1_tx;

// DMA output buffer
uint8_t ws_buffer[LED_BUFFER_LENGTH];

static void encode_byte_9bps(uint8_t data, int16_t buffer_index) {
    static int bits = 0;

    /*
     * use the magic numbers to bitbang the ws2812 protocol
     * honestly, I'm not sure why these are the correct ones
     * but it took me a full day of work to figure them out (:
     */
    const uint32_t zero_zero = 0xF0000F;
    const uint32_t zero_one  = 0xFF000F;
    const uint32_t one_zero  = 0xF0F00F;
    const uint32_t one_one   = 0xFFF00F;

    // go through the 8 bits of data, 2 bits at a time
    int idx = 0;
    for (int i = 3; i >= 0; i--) {
        bits = (data & (0b11 << (i*2))) >> (i*2);
        if (bits == 0b00) {
            memcpy(ws_buffer+buffer_index+(3*idx), &zero_zero, 3);
        } else if (bits == 0b01) {
            memcpy(ws_buffer+buffer_index+(3*idx), &zero_one, 3);
        } else if (bits == 0b10) {
            memcpy(ws_buffer+buffer_index+(3*idx), &one_zero, 3);
        } else if (bits == 0b11) {
            memcpy(ws_buffer+buffer_index+(3*idx), &one_one, 3);
        }
        idx++;
    }
}

static void generate_ws_buffer( uint8_t RData,uint8_t GData,uint8_t BData, int16_t led_no ) {
	//MSB first
	int offset = led_no * 36;

	HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_3);

	// takes about 20us for the full computation
	// i think thats fast enough!

    /* For cheap pcb-mounted WS2812 pixels */
    encode_byte_9bps( GData, offset );
    encode_byte_9bps( RData, offset+12 );
    encode_byte_9bps( BData, offset+24 );

    /* For Sparkfun through-hole pixels */
//  encode_byte_9bps( RData, offset );
//  encode_byte_9bps( GData, offset+12 );
//  encode_byte_9bps( BData, offset+24 );

    HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_3);

}

// initiate the DMA transfer
static void Send_2812(void) {
	// block until SPI ready, then transmit
	while(__HAL_SPI_GET_FLAG(&hspi1, SPI_FLAG_BSY ));
	HAL_SPI_Transmit_DMA( &hspi1, ws_buffer, LED_BUFFER_LENGTH );
}

// sets a pixel to a color along a spectrum
// code is from the Adafruit Neopixel Arduino library
void setPixelHSV(uint16_t n, uint16_t hue, uint8_t sat, uint8_t val) {
	uint8_t r, g, b;

	// Remap 0-360 to 0-1529.
	hue = (hue * 1530L + 180) / 360;
	// Because red is centered on the rollover point (the +32768 above,
	// essentially a fixed-point +0.5), the above actually yields 0 to 1530,
	// where 0 and 1530 would yield the same thing. Rather than apply a
	// costly modulo operator, 1530 is handled as a special case below.

	// So you'd think that the color "hexcone" (the thing that ramps from
	// pure red, to pure yellow, to pure green and so forth back to red,
	// yielding six slices), and with each color component having 256
	// possible values (0-255), might have 1536 possible items (6*256),
	// but in reality there's 1530. This is because the last element in
	// each 256-element slice is equal to the first element of the next
	// slice, and keeping those in there this would create small
	// discontinuities in the color wheel. So the last element of each
	// slice is dropped...we regard only elements 0-254, with item 255
	// being picked up as element 0 of the next slice. Like this:
	// Red to not-quite-pure-yellow is:        255,   0, 0 to 255, 254,   0
	// Pure yellow to not-quite-pure-green is: 255, 255, 0 to   1, 255,   0
	// Pure green to not-quite-pure-cyan is:     0, 255, 0 to   0, 255, 254
	// and so forth. Hence, 1530 distinct hues (0 to 1529), and hence why
	// the constants below are not the multiples of 256 you might expect.

	// Convert hue to R,G,B (nested ifs faster than divide+mod+switch):
	if(hue < 510) {         // Red to Green-1
		b = 0;
		if(hue < 255) {       //   Red to Yellow-1
			r = 255;
			g = hue;            //     g = 0 to 254
		} else {              //   Yellow to Green-1
			r = 510 - hue;      //     r = 255 to 1
			g = 255;
		}
	} else if(hue < 1020) { // Green to Blue-1
		r = 0;
		if(hue <  765) {      //   Green to Cyan-1
			g = 255;
			b = hue - 510;      //     b = 0 to 254
		} else {              //   Cyan to Blue-1
			g = 1020 - hue;     //     g = 255 to 1
			b = 255;
		}
	} else if(hue < 1530) { // Blue to Red-1
		g = 0;
		if(hue < 1275) {      //   Blue to Magenta-1
			r = hue - 1020;     //     r = 0 to 254
			b = 255;
		} else {              //   Magenta to Red-1
			r = 255;
			b = 1530 - hue;     //     b = 255 to 1
		}
	} else {                // Last 0.5 Red (quicker than % operator)
		r = 255;
		g = b = 0;
	}

	// Apply saturation and value to R,G,B, pack into 32-bit result:
	uint32_t v1 =   1 + val; // 1 to 256; allows >>8 instead of /255
	uint16_t s1 =   1 + sat; // 1 to 256; same reason
	uint8_t  s2 = 255 - sat; // 255 to 0
	r = (((((r * s1) >> 8) + s2) * v1) >> 8);
	g = (((((g * s1) >> 8) + s2) * v1) >> 8);
	b = (((((b * s1) >> 8) + s2) * v1) >> 8);

	generate_ws_buffer( r, g, b, n );
	Send_2812();
}

// set every pixel to the given color
void setAllPixelColor(uint8_t r, uint8_t g, uint8_t b)
{
	int i;
	for(i=0;i< LED_NO;i++) {
		generate_ws_buffer( r, g, b, i );
	}
	Send_2812();
}

// set one pixel's color
void setPixelColor(uint16_t n, uint8_t r, uint8_t g, uint8_t b)
{
	generate_ws_buffer( r, g, b, n );
	Send_2812();
}

/**
 * initialize MOSI pin to LOW.  Without this, first time transmit for first LED might be wrong.
 *
 */
void initLEDMOSI(void)
{
	uint8_t buffer0[2] = { 0, 0 };
	HAL_SPI_Transmit(&hspi1, buffer0, 1, 100 );
}

// a blocking delay of the specified number of milliseconds, accompanied by a pretty rainbow :)
// note that this is probably off by a millisecond or two
void rainbowDelay(uint32_t millis) {
	uint32_t startTime = HAL_GetTick();
	uint32_t currentTime = HAL_GetTick();
	uint32_t lastUpdate = HAL_GetTick();
	uint16_t hue = 0;

	while (currentTime - startTime < millis) {
		currentTime = HAL_GetTick();
		if (currentTime - lastUpdate > 5) {
			lastUpdate = currentTime;
			setPixelHSV(0, hue, 255, 100);
			hue = (hue+1)%360;
		}
	}
	setPixelColor(0, 0, 0, 0);
	setPixelColor(0, 0, 0, 0);
}

// private state variables for the LED refresh task
static uint8_t red, green, blue = 0;
uint8_t mode = LED_MODE_SOLID;
uint32_t period = 1000; // flash period in milliseconds
uint32_t lastRefresh = 0; // keep track of the last time the LEDs were updated
uint8_t lastState = 0; // keep track of some state for blinking and pulsing
bool refreshNow = false; // force a refresh after a color change


// setters for the state variables
// change the color
void LED_setColor(uint8_t r, uint8_t g, uint8_t b) {
	if (r!=red || g!=green || b!=blue) {
		red = r;
		green = g;
		blue = b;
		refreshNow = true;
	}
}
// change the mode
void LED_setMode(uint8_t modeIn) {
	if (mode != modeIn) {
		mode = modeIn;
		lastState = 0;
		refreshNow = true;
	}
}
// change the rate
void LED_setRate(uint32_t rate) {
	period = rate;
}

// the main refresh task
void LED_refreshTask() {
	// handle a forced refresh
	// this captures all solid color changes
	if (refreshNow) {
		setPixelColor(0, red, green, blue);
		lastRefresh = HAL_GetTick();
		refreshNow = false;
	}

	// handle blinking
	if (mode == LED_MODE_BLINK) {
		uint32_t timeNow = HAL_GetTick();
		if (timeNow - lastRefresh >= (period/2)) {
			lastRefresh = timeNow;
			if (lastState == 0) {
				setPixelColor(0, red, green, blue);
				lastState = 1;
			} else if (lastState == 1) {
				setPixelColor(0, 0, 0, 0);
				lastState = 0;
			}
		}
	}

	// handle pulsing
	if (mode == LED_MODE_FADE) {
		uint32_t timeNow = HAL_GetTick();
		if (timeNow - lastRefresh >= (period/100)) {
			uint32_t rFade, gFade, bFade;
			lastRefresh = timeNow;
			// states 0 to 100 are upward slope
			if (lastState <= 100) {
				// WATCH OUT FOR THE OVERFLOW!!!!!!!!!!
				rFade = (red * lastState)/100;
				gFade = (green * lastState)/100;
				bFade = (blue * lastState)/100;
				setPixelColor(0, rFade, gFade, bFade);
			// states 100 to 200 are downward slope
			} else {
				rFade = (red * (200 - lastState))/100;
				gFade = (green * (200 - lastState))/100;
				bFade = (blue * (200 - lastState))/100;
				setPixelColor(0, rFade, gFade, bFade);
			}

			lastState = (lastState + 1)%201;
		}
	}
}


