/*
 * log.c
 *
 *  Created on: Aug 29, 2020
 *      Author: annie
 */

#include "log.h"
#include "neopix.h"
#include <string.h>

UART_HandleTypeDef huart2;
DMA_HandleTypeDef hdma_usart2_tx;

bool logNameSet = false;
bool loggingActive = true;

char logName[9];
char dataName[9];

uint32_t ctr = 0;
bool startedFile = false;
static char filName[9];
static char header[] = "iTOW,UTC,tAcc,Lon,Lat,Height,HMSL,hAcc,vAcc,pDOP,\
numSV,velN,velE,velD,gspeed,sAcc,headmot,headAcc,fixType,fixOK,\
UTCvalid,HPvalid,carrierSoln,diffCorrected,pitch,roll,yaw,\
jamState1,noise1,agc1,jamInd1,jamState2,noise2,agc2,jamInd2\r\n";

char * toWrite;

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	HAL_GPIO_TogglePin(DEB2_GPIO_Port, DEB2_Pin);
	HAL_GPIO_TogglePin(DEB2_GPIO_Port, DEB2_Pin);
	HAL_GPIO_TogglePin(DEB2_GPIO_Port, DEB2_Pin);
	HAL_GPIO_TogglePin(DEB2_GPIO_Port, DEB2_Pin);
	HAL_GPIO_TogglePin(DEB2_GPIO_Port, DEB2_Pin);
	HAL_GPIO_TogglePin(DEB2_GPIO_Port, DEB2_Pin);
}

// output buffer for running at high data rates
// note that this uses A LOT of memory!!!!
// specifically, DATA_BUF_SIZE*255 bytes (1.3KB if DATA_BUF_SIZE = 5)
//static char outBuf[DATA_BUF_SIZE][255];
uint8_t outBufIdx = 0;

void sd_openfile(char * fname) {
	uint8_t esc = 26;
	char syncStr[] = "\r\n\r\nsync\r\n"; // 10
	char namingStr[] = "\r\n\r\n\r\nappend stringgg.txt\r\n"; //27

	// copy over the name
	strncpy(namingStr+13, fname, 12);

	// send the escape sequence
	HAL_UART_Transmit(&huart2, &esc, 1, 100);
	HAL_UART_Transmit(&huart2, &esc, 1, 100);
	HAL_UART_Transmit(&huart2, &esc, 1, 100);

	HAL_Delay(100);

	// sync (in case we have an old file running
	HAL_UART_Transmit(&huart2, syncStr, 10, 100);
	HAL_Delay(100);

	// send the file name
	while(HAL_UART_GetState(&huart2) == HAL_UART_STATE_BUSY );
	//HAL_UART_Transmit_DMA(&huart3, namingStr, 21);
	HAL_UART_Transmit_DMA(&huart2, namingStr, 27);

	HAL_Delay(100);

}

void roverLoggerTask() {
	// wait until we get our first valid GPS string and use its timestamp as the file name
	FLAG_UBX_PARSED = 0; // reset the flag, we're handling this packet
	HAL_GPIO_TogglePin(DEB0_GPIO_Port, DEB0_Pin);
	UBXCheckPackets();
	if (!startedFile && loggingActive && UBXIsNextValid()) {
		filName[8]=0;

		// set startedFile to true to escape this while section
		startedFile = true;

//		// generate the string
//		buildString();
//		toWrite = getString();

		// unpack the timestamp from the CSV string
		char * startPtr = strchr(ubxIn.CSVString, ',');
		startPtr+=1;

		// make an MS-DOS/FAT compatible filename (MMDDHHMM.CSV)
		filName[0] = startPtr[5]; filName[1] = startPtr[6]; // month
		filName[2] = startPtr[8]; filName[3] = startPtr[9]; // day
		filName[4] = startPtr[11]; filName[5] = startPtr[12]; // hour
		filName[6] = startPtr[14]; filName[7] = startPtr[15]; // minute

		// create the file, write the header and the first data line
		log_setName(filName);

		// write the timepulse settings
		toWrite = getTPString();
		log_writeLine(toWrite);

		// write the header
		log_writeLine(header);

		// generate and write the string
		buildString();
		toWrite = getString();
		//log_writeLine(toWrite); // miss the first line, fixes the leading null error
	}
	// if we already have a file going, keep writing to it
	else if (UBXIsNextValid()) {
		// generate the string
		buildString();
		toWrite = getString();
		FLAG_STRING_READY = 0; // reset the string ready flag
		if (toWrite != 0) {
			log_writeLine(toWrite);

			ctr++;//DATA_BUF_SIZE;
			if (ctr >= MAX_FILE_LEN) {
				// start a new file on the next round
				ctr = 0;
				startedFile = false;
			}
			//				}

		}
	} else {
		// just to advance the tail
		buildString();
		getString();
		FLAG_STRING_READY = 0;
	}
	// end else
	HAL_GPIO_TogglePin(DEB0_GPIO_Port, DEB0_Pin);
	//HAL_Delay(200);
} // end task

// set the name of the logfile
void log_setName(char * fname) {
	// copy over the first 8 characters in the name
	strncpy(logName, fname, 8);
#ifdef ROLE_BASE
	logName[7] = 'L';
#endif
	logName[8] = 0;

	// make the log file
	sd_openfile(logName);

	// tell everyone else we've set the logName
	logNameSet = true;
}

// logs a line to the log file
void log_writeLine(char * lineIn) {
	HAL_GPIO_TogglePin(DEB3_GPIO_Port, DEB3_Pin);
	if (logNameSet) {
		while(huart2.gState != HAL_UART_STATE_READY);
		HAL_UART_Transmit_DMA(&huart2, lineIn, strlen(lineIn));

	}
	HAL_GPIO_TogglePin(DEB3_GPIO_Port, DEB3_Pin);
}

// sends the sync command and stops logging
void log_sendSync() {

    // unset this flag to stop logging new data lines
    loggingActive = false;

    // put the openlog into command mode
    uint8_t esc = 26;
    HAL_UART_Transmit(&huart2, &esc, 1, 100);
    HAL_Delay(10);
    HAL_UART_Transmit(&huart2, &esc, 1, 100);
    HAL_Delay(10);
    HAL_UART_Transmit(&huart2, &esc, 1, 100);
    HAL_Delay(100);

    // clear the "unknown command" error if it happens
    HAL_UART_Transmit(&huart2, "\r\n", 2, 100);
    HAL_Delay(50);

    // send the sync command
    char syncStr[] = "\r\nsync\r\n";
    HAL_UART_Transmit(&huart2, syncStr, 8, 100);
}


