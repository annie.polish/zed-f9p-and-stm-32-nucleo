/*
 * ubx.c
 *
 *  Created on: Jun 26, 2020
 *      Author: annie
 */

#include "ubx.h"
#include "ringbuf.h"
#include "BNO055.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

uint8_t FLAG_UBX_READY = 0;  // a UBX packet is waiting on the UART buffer
uint8_t FLAG_UBX_PARSED = 0; // a UBX packet has arrived and is in ubxIn
uint8_t FLAG_STRING_READY=0; // a UBX packet has been converted to a string and is ready to write to SD card

uint8_t FLAG_SURVEY_STATUS=0;

// public variables for accessing updated UBX data
bool UBX_busy = false;
uint32_t UBX_thisTick = 0;
uint32_t UBX_lastTick = 0;
uint8_t UBX_diffStatus = 0;
uint32_t UBX_carrierStatus = 0;
uint32_t UBX_time = 0;
uint32_t UBX_iTOW = 0;
bool UBX_locked = false;
int UBX_lat = 0;
int UBX_lon = 0;
uint8_t UBX_numSV = 0;
bool UBX_svinActive = false;
uint32_t UBX_svinProg = 0;
uint32_t UBX_svinAcc = 99999999;
uint32_t UBX_hAcc = 99999999;
uint32_t UBX_fullyResolved = 0;
char tpString[] = "TPPER=unknown   , TPLEN=unknown   \r\n";

// now without a ring buffer
ubxLine ubxIn; // the actual UBX data
bool UBX_validated = false; // has this packet been verified?
// FLAG_UBX_READY monitors data reading
// FLAG_STRING_READY monitors data logging


// the config buffer
uint8_t cfgBuf[12 + 12*MAX_CFG_PAIRS];
bool cfgInitialized = false;
uint32_t cfgIdx = 0;

// private function prototypes
static void parseHPPOSLLH(HPPOSLLH * pkt);
static void parsePVT(PVT * pkt);
static void parseSVIN(SVIN * pkt);
static void parseACK(ACK * pkt);
static void parseRF(RF * pkt);
static uint16_t readUShort();
static int16_t readShort();
static uint32_t readUInt();
static int32_t readInt();
static void doSum(void * data_in, uint8_t * CK_A, uint8_t * CK_B);
static void validateRover();
#ifdef ROLE_BASE
static void validateBase();
#endif

// public functions

// the IDLE line ISR function for Rover packets
void UBXIdle() {
	HAL_GPIO_TogglePin(DEB2_GPIO_Port, DEB2_Pin);

	uint8_t class, ID, data = 0;
	uint16_t length = 0;
	int state = SM_UBX_BEFORE;
	// while IsDataAvailable():
	while(IsDataAvailable()) {
		data = Uart_read();
		// main state machine:
		// look for the first sync character
		if (state == SM_UBX_BEFORE && data == UBX_SYN_CHAR1) {
			state = SM_UBX_SYN2;
		}
		// look for the second sync character
		else if (state == SM_UBX_SYN2 && data == UBX_SYN_CHAR2) {
			state = SM_UBX_CLASS;
		}
		// parse the class
		else if (state == SM_UBX_CLASS) {
			class = data;
			state = SM_UBX_ID;
		}
		// parse the ID
		else if (state == SM_UBX_ID) {
			ID = data;
			state = SM_UBX_PAYLEN1;
		}
		// parse the length
		else if (state == SM_UBX_PAYLEN1) {
			length = data;
			state = SM_UBX_PAYLEN2;
		} else if (state == SM_UBX_PAYLEN2) {
			length |= data<<8;
			state = SM_UBX_PAYLOAD;
		}

		// parse the payload
		if (state == SM_UBX_PAYLOAD) {
			if (class == UBX_NAV && ID == UBX_NAV_PVT) {
				ubxIn.pvt.class = class;
				ubxIn.pvt.ID = ID;
				ubxIn.pvt.length = length;
				parsePVT(&(ubxIn.pvt));
				ubxIn.pvt.CK_A = Uart_read();
				ubxIn.pvt.CK_B = Uart_read();
			} else if (class == UBX_NAV && ID == UBX_NAV_HPPOSLLH) {
				ubxIn.hpposllh.class = class;
				ubxIn.hpposllh.ID = ID;
				ubxIn.hpposllh.length = length;
				parseHPPOSLLH(&(ubxIn.hpposllh));
				ubxIn.hpposllh.CK_A = Uart_read();
				ubxIn.hpposllh.CK_B = Uart_read();
			} else if (class == UBX_NAV && ID == UBX_NAV_SVIN) {
				ubxIn.svin.class = class;
				ubxIn.svin.ID = ID;
				ubxIn.svin.length = length;
				parseSVIN(&(ubxIn.svin));
				ubxIn.svin.CK_A = Uart_read();
				ubxIn.svin.CK_B = Uart_read();
			} else if (class == UBX_MON && ID == UBX_MON_RF) {
				ubxIn.rf.class = class;
				ubxIn.rf.ID = ID;
				ubxIn.rf.length = length;
				parseRF(&(ubxIn.rf));
				ubxIn.rf.CK_A = Uart_read();
				ubxIn.rf.CK_B = Uart_read();
			}
			state = SM_UBX_BEFORE;
		}
	}

	// increment and wrap the buffer head
	//ubxHead = (ubxHead + 1) % BUF_SIZE;
	FLAG_UBX_READY = 0;
	FLAG_STRING_READY = 0;
	FLAG_UBX_PARSED = 1; // the packet has been parsed
	UBX_validated = false;

	// update the timer
	UBX_lastTick = UBX_thisTick;
	UBX_thisTick = HAL_GetTick();
	HAL_GPIO_TogglePin(DEB2_GPIO_Port, DEB2_Pin);
}

// checks the checksums of the next packet bundle
void UBXCheckPackets() {
	// make sure there's something in the buffer
	if (UBX_validated) return;

	// check the checksums of both packets
	bool allValid = true;

	uint8_t CK_A = 0; uint8_t CK_B = 0;
	doSum(&ubxIn.pvt, &CK_A, &CK_B);
	if (CK_A != ubxIn.pvt.CK_A || CK_B != ubxIn.pvt.CK_B) {
		allValid = false;
	}

	CK_A = 0; CK_B = 0;
	doSum(&ubxIn.hpposllh, &CK_A, &CK_B);
	if (CK_A != ubxIn.hpposllh.CK_A || CK_B != ubxIn.hpposllh.CK_B) {
		allValid = false;
	}
	validateRover();

	// set the checksum valid flag
	ubxIn.chkValid = allValid;
	UBX_validated = true;
}

// returns true if and only if the next packet contains good data
bool UBXIsNextValid() {
	return ubxIn.chkValid && ubxIn.dataValid;
}

// builds the next string in the buffer
int buildString() {
	// make sure we haven't made this string already
	if (FLAG_STRING_READY) return -1;

	// go get the string we'll be writing to
	char * str = ubxIn.CSVString;
	int idx = 0;

	// start copying values into the string

	// GPS time of week
	idx += sprintf(str, "%lu,", ubxIn.pvt.iTOW);

	// UTC timestamp string
	// calculate the sub-second timestamp using the nano field
	int micros = ubxIn.pvt.nano / 1000;
	int secs   = ubxIn.pvt.sec;
	int mins   = ubxIn.pvt.min;
	int hours  = ubxIn.pvt.hour;
	int days   = ubxIn.pvt.day;
	int months = ubxIn.pvt.month;
	int years  = ubxIn.pvt.year;

	// this gets complicated when nano is negative
	if (micros < 0) {
		micros = micros + 1000000; // micros *should* be positive
		secs = secs - 1; // decrement the seconds
		if (secs < 0) { // make sure seconds haven't wrapped
			secs = secs + 60;
			mins = mins - 1;
			if (mins < 0) { // make sure minutes haven't wrapped
				mins = mins + 60;
				hours = hours - 1;
				if (hours < 0) { // make sure hours haven't wrapped
					hours = hours + 24;
					days = days - 1;
					if (days < 0) { // make sure days haven't wrapped
						months = months - 1;
						if (months==2) {
							// fuck february tbh
							if (years%4 == 0 && years%100 != 0) {
								// leap year leap year leap year wooooooo
								days = 28;
							} else {
								days = 29;
							}
						} else if (months==9 || months==4 || months==6 || months==11) {
							days = 30; // 30 days has september, april, june, and november
						} else {
							days = 31; // all the rest have 31
						}
						if (months < 0) { // make sure months haven't wrapped
							months = months + 12;
							years = years - 1;
							if (years < 0) { // finally, make sure years haven't wrapped
								// this will only matter in 2099 :/
								years = years + 100;
							}
						}
					}
				}
			}
		}
	}

	idx += sprintf(str+idx, "%u-%02u-%02uT%02u:%02u:%02u.%06uZ,",
			years, months, days, hours,	mins, secs, micros);

	// tAcc
	idx += sprintf(str+idx, "%lu,", ubxIn.pvt.tAcc);

	// Longitude
	int lonUnits = ubxIn.hpposllh.lon/10000000;
	int lonDec = (ubxIn.hpposllh.lon - lonUnits*10000000) * 100;
	lonDec += ubxIn.hpposllh.lonHp;
	if (lonUnits == 0 && lonDec < 0) { idx+=sprintf(str+idx, "-%d.%09u,",lonUnits,lonDec*-1); }
	else if (lonDec<0) { idx+=sprintf(str+idx, "%d.%09u,",lonUnits,lonDec*-1); }
	else { idx+=sprintf(str+idx, "%d.%09u,",lonUnits,lonDec); }

	// Latitude
	int latUnits = ubxIn.hpposllh.lat/10000000;
	int latDec = (ubxIn.hpposllh.lat - latUnits*10000000) * 100;
	latDec += ubxIn.hpposllh.latHp;
	if (latUnits == 0 && latDec < 0) { idx+=sprintf(str+idx, "-%d.%09u,",latUnits,latDec*-1); }
	else if (latDec<0) { idx+=sprintf(str+idx, "%d.%09u,",latUnits,latDec*-1); }
	else { idx+=sprintf(str+idx, "%d.%09u,",latUnits,latDec); }

	// Height above geoid
	int heightUnits = ubxIn.hpposllh.height/1000;
	int heightDec = (ubxIn.hpposllh.height - heightUnits*1000) * 10;
	heightDec += ubxIn.hpposllh.heightHp;
	if (heightUnits == 0 && heightDec < 0) { idx+=sprintf(str+idx, "-%d.%04u,",heightUnits,heightDec*-1); }
	else if (heightDec<0) { idx+=sprintf(str+idx, "%d.%04u,",heightUnits,heightDec*-1); }
	else { idx+=sprintf(str+idx, "%d.%04u,",heightUnits,heightDec); }

	// Height above mean sea level
	int hmslUnits = ubxIn.hpposllh.hMSL/1000;
	int hmslDec = (ubxIn.hpposllh.hMSL - hmslUnits*1000) * 10;
	hmslDec += ubxIn.hpposllh.hMSLHp;
	if (hmslUnits == 0 && hmslDec < 0) { idx+=sprintf(str+idx, "-%d.%04u,",hmslUnits,hmslDec*-1); }
	else if (hmslDec<0) { idx+=sprintf(str+idx, "%d.%04u,",hmslUnits,hmslDec*-1); }
	else { idx+=sprintf(str+idx, "%d.%04u,",hmslUnits,hmslDec); }

	// accuracy, dilution of precision, and number of satellites
	idx += sprintf(str+idx, "%lu,%lu,%3u,%u,", ubxIn.hpposllh.hAcc, ubxIn.hpposllh.vAcc,
			ubxIn.pvt.pDOP, ubxIn.pvt.numSV);

	// North velocity
	int nUnits = ubxIn.pvt.velN/1000;
	int nDec = ubxIn.pvt.velN - nUnits*1000;
	if (nUnits == 0 && nDec < 0) { idx+=sprintf(str+idx, "-%d.%03u,",nUnits,nDec*-1); }
	else if (nDec<0) { idx+=sprintf(str+idx, "%d.%03u,",nUnits,nDec*-1); }
	else { idx+=sprintf(str+idx, "%d.%03u,",nUnits,nDec); }

	// East velocity
	int eUnits = ubxIn.pvt.velE/1000;
	int eDec = ubxIn.pvt.velE - eUnits*1000;
	if (eUnits == 0 && eDec < 0) { idx+=sprintf(str+idx, "-%d.%03u,",eUnits,eDec*-1); }
	else if (eDec<0) { idx+=sprintf(str+idx, "%d.%03u,",eUnits,eDec*-1); }
	else { idx+=sprintf(str+idx, "%d.%03u,",eUnits,eDec); }

	// Down velocity
	int dUnits = ubxIn.pvt.velD/1000;
	int dDec = ubxIn.pvt.velD - dUnits*1000;
	if (dUnits == 0 && dDec < 0) { idx+=sprintf(str+idx, "-%d.%03u,",dUnits,dDec*-1); }
	else if (dDec<0) { idx+=sprintf(str+idx, "%d.%03u,",dUnits,dDec*-1); }
	else { idx+=sprintf(str+idx, "%d.%03u,",dUnits,dDec); }

	// Ground speed
	int sUnits = ubxIn.pvt.gSpeed/1000;
	int sDec = ubxIn.pvt.gSpeed - sUnits*1000;
	if (sUnits == 0 && sDec < 0) { idx+=sprintf(str+idx, "-%d.%03u,",sUnits,sDec*-1); }
	else if (sDec<0) { idx+=sprintf(str+idx, "%d.%03u,",sUnits,sDec*-1); }
	else { idx+=sprintf(str+idx, "%d.%03u,",sUnits,sDec); }

	// Ground speed accuracy
	int saccUnits = ubxIn.pvt.sAcc/1000;
	int saccDec = ubxIn.pvt.sAcc - saccUnits*1000;
	idx+=sprintf(str+idx, "%d.%03u,",saccUnits,saccDec);

	// Heading
	int headUnits = ubxIn.pvt.headMot/100000;
	int headDec = ubxIn.pvt.headMot - headUnits*100000;
	if (headUnits == 0 && headDec < 0) { idx+=sprintf(str+idx, "-%d.%05u,",headUnits,headDec*-1); }
	else if (headDec<0) { idx+=sprintf(str+idx, "%d.%05u,",headUnits,headDec*-1); }
	else { idx+=sprintf(str+idx, "%d.%05u,",headUnits,headDec); }

	// Heading accuracy
	int haccUnits = ubxIn.pvt.headAcc/100000;
	int haccDec = ubxIn.pvt.headAcc - haccUnits*100000;
	idx+=sprintf(str+idx, "%d.%05u,",haccUnits,haccDec);

	// errors and flags
	int hppos_valid = !(ubxIn.hpposllh.flags & 1);
	int utc_flags = ubxIn.pvt.flags2;
	int utc_valid = (utc_flags & 1<<7) && (utc_flags & 1<<6) && (utc_flags & 1<<5);
	int fix_valid = ubxIn.pvt.flags & 1;
	int diff_valid = ubxIn.pvt.flags & (1<<1);
	int carrier_soln = (ubxIn.pvt.flags & (3 << 6)) >> 6;
	idx += sprintf(str+idx, "%u,%u,%u,%u,%u,%u,",ubxIn.pvt.fixType,fix_valid,utc_valid,hppos_valid,carrier_soln,diff_valid);

	// imu data
	int pitchUnits, pitchDec, rollUnits, rollDec, yawUnits, yawDec;
	pitchUnits = IMU_pitch/10000;
	rollUnits = IMU_roll/10000;
	yawUnits = IMU_yaw/10000;
	pitchDec = abs(IMU_pitch-(pitchUnits*10000));
	rollDec = abs(IMU_roll-(rollUnits*10000));
	yawDec = abs(IMU_yaw-(yawUnits*10000));
	idx += sprintf(str+idx, "%d.%u,%d.%u,%d.%u",pitchUnits,pitchDec,rollUnits,rollDec,yawUnits,yawDec);

	// RF data
	int jamState, noise, agcCnt, jamInd;
	// block 1
	jamState = ubxIn.rf.b1_flags;
	noise = ubxIn.rf.b1_noisePerMS;
	agcCnt = ubxIn.rf.b1_agcCnt;
	jamInd = ubxIn.rf.b1_jamInd;
	idx += sprintf(str+idx, ",%u,%u,%u,%u",jamState,noise,agcCnt,jamInd);
	// block 2
	jamState = ubxIn.rf.b2_flags;
	noise = ubxIn.rf.b2_noisePerMS;
	agcCnt = ubxIn.rf.b2_agcCnt;
	jamInd = ubxIn.rf.b2_jamInd;
	idx += sprintf(str+idx, ",%u,%u,%u,%u",jamState,noise,agcCnt,jamInd);

	// the end of the string
	sprintf(str+idx, "\r\n");

	// increment and wrap the buffer tail
	//ubxStrTail = (ubxStrTail + 1) % BUF_SIZE;
	FLAG_STRING_READY = 1; // flip the string ready flag
	return idx;
}

// returns the next available CSV string
char * getString() {
	// make sure there's something in the buffer
	//if (ubxTail == ubxHead) return (char *)0;
	if (!FLAG_STRING_READY) return (char *)0;
	char * retval = ubxIn.CSVString;
	// increment the real tail; we're done with this packet
	//ubxTail = (ubxTail + 1) % BUF_SIZE;
	return retval;
}

// build and returns a timepulse settings string
char * getTPString() {
	return tpString;
}

// sends a SVIN config packet
void sendStartSurvey(uint32_t dur, uint32_t acc) {
	uint8_t mode = 1;
	uint8_t dynmode = 2;

	cfg_clear();
	cfg_add(CFG_TMODE_MODE, &mode, 1);
	cfg_add(CFG_TMODE_SVIN_MIN_DUR, &dur, 4);
	cfg_add(CFG_TMODE_SVIN_ACC_LIM, &acc, 4);
	cfg_add(CFG_NAVSPG_DYNMODEL, &dynmode, 1);
	cfg_transmit();
}

// sends a location lock config packet
void sendLockLocation(int32_t lat, int32_t lon, int32_t height) {
	uint8_t mode = 2; // fixed
	uint8_t posType = 1; // llh

	cfg_clear();
	cfg_add(CFG_TMODE_MODE, &mode, 1);
	cfg_add(CFG_TMODE_LON, &lon, 4);
	cfg_add(CFG_TMODE_LAT, &lat, 4);
	cfg_add(CFG_TMODE_HEIGHT, &height, 4);
	cfg_add(CFG_TMODE_POS_TYPE, &posType, 1);
	cfg_transmit();
}

// sends a timepulse config packet
void sendTPConfig(uint32_t period, uint32_t len) {
	uint8_t one = 1;
	uint8_t zero = 0;

	// logs the fact that the timepulse was configured
	// char tpString[] = "TPPER=unknown   , TPLEN=unknown   \r\n";
	sprintf(tpString, "TPPER=%10u, TPLEN=%10u\r\n", period,len);

	cfg_clear();
	cfg_add(CFG_TP_USE_LOCKED_TP1, &zero, 1);
	cfg_add(CFG_TP_PULSE_DEF, &zero, 1);
	cfg_add(CFG_TP_PULSE_LENGTH_DEF, &one, 1);
	cfg_add(CFG_TP_PERIOD_TP1, &period, 4);
	cfg_add(CFG_TP_LEN_TP1, &len, 4);
	cfg_transmit();
}

// sends a packet to configure the base station in its default configuration
void sendBaseConfig() {
	uint8_t one = 1;
	uint8_t zero = 0;
	uint8_t mode = 0; // TMODE disabled
	//uint8_t dynmode = 2; //stationary dynamic model
	uint8_t dynmode = 0; //portable dynamic model
	uint32_t baudRate = 115200;

	cfg_clear();

	// set up some basic GPS config stuff
	cfg_add(CFG_TMODE_MODE, &mode, 1);
	cfg_add(CFG_NAVSPG_DYNMODEL, &dynmode, 1);
	cfg_add(CFG_SIGNAL_GAL_ENA, &one, 1);
	cfg_add(CFG_SIGNAL_GLO_ENA, &one, 1);
	cfg_add(CFG_SIGNAL_SBAS_ENA, &one, 1);
	cfg_add(CFG_SIGNAL_BDS_ENA, &zero, 1);

	// set up the protocols and interfaces
	//cfg_add(CFG_UART1_BAUDRATE, &baudRate, 4);
	cfg_add(CFG_UART2_BAUDRATE, &baudRate, 4);
	cfg_add(CFG_UART1OUTPROT_UBX, &one, 1);
	//cfg_add(CFG_UART1OUTPROT_UBX, &zero, 1);
	cfg_add(CFG_UART2INPROT_NMEA, &one, 1);
	cfg_add(CFG_UART2OUTPROT_RTCM3X, &one, 1);

	// RTCM correction messages
	cfg_add(CFG_MSGOUT_RTCM_3X_TYPE1005_UART2, &one, 1);
	cfg_add(CFG_MSGOUT_RTCM_3X_TYPE1074_UART2, &one, 1);
	cfg_add(CFG_MSGOUT_RTCM_3X_TYPE1084_UART2, &one, 1);
	cfg_add(CFG_MSGOUT_RTCM_3X_TYPE1094_UART2, &one, 1);
	cfg_add(CFG_MSGOUT_RTCM_3X_TYPE1230_UART2, &one, 1);

	// base monitoring messages
	cfg_add(CFG_MSGOUT_UBX_NAV_PVT_UART1, &one, 1);
	cfg_add(CFG_MSGOUT_UBX_NAV_SVIN_UART1, &one, 1);

	cfg_transmit();
}


// sends a packet to configure the rover in its default configuration
void sendRoverConfig() {
	uint8_t one = 1;
	uint8_t zero = 0;
	uint8_t mode = 0; // TMODE disabled
	//uint8_t dynmode = 7; // airborne dynamic model
	uint8_t dynmode = 0; // portable dynamic model
	uint32_t baudRate = 115200;

	cfg_clear();

	// set up some basic GPS config stuff
	cfg_add(CFG_TMODE_MODE, &mode, 1);
	cfg_add(CFG_NAVSPG_DYNMODEL, &dynmode, 1);
	cfg_add(CFG_SIGNAL_GAL_ENA, &one, 1);
	cfg_add(CFG_SIGNAL_GLO_ENA, &one, 1);
	cfg_add(CFG_SIGNAL_SBAS_ENA, &zero, 1);
	cfg_add(CFG_SIGNAL_BDS_ENA, &zero, 1);

	// set up the protocols and interfaces
	cfg_add(CFG_UART2_BAUDRATE, &baudRate, 4);
	cfg_add(CFG_UART1OUTPROT_UBX, &one, 1);
	//cfg_add(CFG_UART2OUTPROT_NMEA, &one, 1);
	//cfg_add(CFG_UART2OUTPROT_NMEA, &zero, 1);
	cfg_add(CFG_UART2OUTPROT_RTCM3X, &zero, 1);
	cfg_add(CFG_UART2INPROT_RTCM3X, &one, 1);

	// logging messages
	cfg_add(CFG_MSGOUT_UBX_NAV_PVT_UART1, &one, 1);
	cfg_add(CFG_MSGOUT_UBX_NAV_HPPOSLLH_UART1, &one, 1);

	// turn on the RF logging packet
	cfg_add(CFG_MSGOUT_UBX_MON_RF_UART1, &one, 1);

	// base monitoring messages
	cfg_add(CFG_MSGOUT_NMEA_ID_GGA_UART2, &zero, 1);
	cfg_add(CFG_MSGOUT_NMEA_ID_GLL_UART2, &zero, 1);
	cfg_add(CFG_MSGOUT_NMEA_ID_GSA_UART2, &zero, 1);
	cfg_add(CFG_MSGOUT_NMEA_ID_GSV_UART2, &zero, 1);
	cfg_add(CFG_MSGOUT_NMEA_ID_RMC_UART2, &zero, 1);
	cfg_add(CFG_MSGOUT_NMEA_ID_VTG_UART2, &zero, 1);
	//cfg_add(CFG_MSGOUT_PUBX_ID_POLYP_UART2, &one, 1);
	//cfg_add(CFG_MSGOUT_PUBX_ID_POLYP_UART2, &zero, 1);

	cfg_transmit();
}

// turns rover telemetry feed on or off
// on is better for safety (this packet is what recovered our runaway drone)
// but the telemetry link contributes to the drone's RFI
void sendTelemConfig(bool setting) {
	cfg_clear();
	cfg_add(CFG_MSGOUT_PUBX_ID_POLYP_UART2, &setting, 1);
	//cfg_add(CFG_UART2OUTPROT_NMEA, &setting, 1);
	cfg_transmit();
}

// adds a cfgval to the building cfg_buf
bool cfg_add(uint32_t key, uint8_t * value, uint8_t size) {
	// check if this would overflow the buffer
	if (cfgIdx + 4 + size >= 10+(12*MAX_CFG_PAIRS)) {
		return false;
	}

	// initialize the buffer if needed
	if (!cfgInitialized) {
		// magic number and CFG-VALSET values
		cfgBuf[0] = 0xB5; cfgBuf[1] = 0x62;
		cfgBuf[2] = 0x06; cfgBuf[3] = 0x8a;

		// zero out the length
		cfgBuf[4] = 0; cfgBuf[5] = 0;

		// layer (xx1 ram, x1x bbm, 1xx flash)
		cfgBuf[7] = 0b001;

		// reserved fields
		cfgBuf[6] = 0; cfgBuf[8] = 0; cfgBuf[9] = 0;

		cfgIdx = 10;
		cfgInitialized = true;
	}

	// copy in the key
	memcpy(&(cfgBuf[cfgIdx]), &key, 4); cfgIdx += 4;

	// copy in the value
	memcpy(&(cfgBuf[cfgIdx]), value, size); cfgIdx += size;

	return true;
}

// clears the cfg_buf
void cfg_clear() {
	cfgInitialized = false;
	cfgIdx = 0;
}

// sends the cfg buffer
void cfg_transmit() {
	if (!cfgInitialized) return; // if it's empty, do nothing

	// compute the length
	uint16_t len = cfgIdx - 6;
	memcpy(&(cfgBuf[4]), &len, 2);

	// compute the checksum
	uint8_t CK_A, CK_B;
	doSum(cfgBuf+2, &(CK_A), &CK_B);
	cfgBuf[cfgIdx++] = CK_A;
	cfgBuf[cfgIdx] = CK_B;

	// actually send the packet
	for (int i = 0; i < len+8; i++) {
		Uart_write(cfgBuf[i]);
	}
}


// private functions

// parser functions
// these functions should only be called when you believe the next packet read by Read_uart()
// will be the first packet in the respective type of payload

static void parseHPPOSLLH(HPPOSLLH * pkt) {
	pkt->version = Uart_read();
	pkt->reserved = readUShort();
	pkt->flags = Uart_read();
	pkt->iTOW = readUInt();
	pkt->lon = readInt();
	pkt->lat = readInt();
	pkt->height = readInt();
	pkt->hMSL = readInt();
	pkt->lonHp = Uart_read();
	pkt->latHp = Uart_read();
	pkt->heightHp = Uart_read();
	pkt->hMSLHp = Uart_read();
	pkt->hAcc = readUInt();
	pkt->vAcc = readUInt();
	UBX_hAcc = pkt->hAcc;
}

static void parsePVT(PVT * pkt) {
	pkt->iTOW = readUInt();
	pkt->year = readUShort();
	pkt->month = Uart_read();
	pkt->day = Uart_read();
	pkt->hour = Uart_read();
	pkt->min = Uart_read();
	pkt->sec = Uart_read();
	pkt->valid = Uart_read();
	pkt->tAcc = readUInt();
	pkt->nano = readInt();
	pkt->fixType = Uart_read();
	pkt->flags = Uart_read();
	pkt->flags2 = Uart_read();
	pkt->numSV = Uart_read();
	pkt->lon = readInt();
	pkt->lat = readInt();
	pkt->height = readInt();
	pkt->hMSL = readInt();
	pkt->hAcc = readUInt();
	pkt->vAcc = readUInt();
	pkt->velN = readInt();
	pkt->velE = readInt();
	pkt->velD = readInt();
	pkt->gSpeed = readInt();
	pkt->headMot = readInt();
	pkt->sAcc = readUInt();
	pkt->headAcc = readUInt();
	pkt->pDOP = readUShort();
	pkt->flags3 = Uart_read();
	pkt->reserved[0] = Uart_read();
	pkt->reserved[1] = Uart_read();
	pkt->reserved[2] = Uart_read();
	pkt->reserved[3] = Uart_read();
	pkt->reserved[4] = Uart_read();
	pkt->headVeh = readInt();
	pkt->magDec = readShort();
	pkt->macAcc = readUShort();

	// update the public variables
	UBX_carrierStatus = ((pkt->flags) & (3 << 6)) >> 6;
	UBX_diffStatus = (pkt->flags) & 1<<1;
	UBX_time = (pkt->hour)*10000 + (pkt->min)*100 + pkt->sec;
	UBX_locked = pkt->fixType == 5;
	UBX_lat = pkt->lat;
	UBX_lon = pkt->lon;
	UBX_numSV = pkt->numSV;
	UBX_iTOW = pkt->iTOW;
	UBX_fullyResolved = (pkt->valid) & 1<<2;

}

static void parseSVIN(SVIN * pkt) {
	pkt->version = Uart_read();
	pkt->reserved[0] = Uart_read();
	pkt->reserved[1] = Uart_read();
	pkt->reserved[2] = Uart_read();
	pkt->iTOW = readUInt();
	pkt->dur = readUInt();
	pkt->meanX = readInt();
	pkt->meanY = readInt();
	pkt->meanZ = readInt();
	pkt->meanXHP = Uart_read();
	pkt->meanYHP = Uart_read();
	pkt->meanZHP = Uart_read();
	pkt->reserved1 = Uart_read();
	pkt->meanAcc = readUInt();
	pkt->obs = readUInt();
	pkt->valid = Uart_read();
	pkt->active = Uart_read();
	pkt->reserved2 = readUShort();
}

static void parseRF(RF * pkt) {
	pkt->version = Uart_read();
	pkt->nBlocks = Uart_read(); // should be 2, otherwise this structure breaks
	pkt->reserved[0] = Uart_read();
	pkt->reserved[1] = Uart_read();
	pkt->b1_blockId = Uart_read();
	pkt->b1_flags = Uart_read();
	pkt->b1_antStatus = Uart_read();
	pkt->b1_antPower = Uart_read();
	pkt->b1_postStatus = readUInt();
	pkt->b1_reserved1[0] = Uart_read();
	pkt->b1_reserved1[1] = Uart_read();
	pkt->b1_reserved1[2] = Uart_read();
	pkt->b1_reserved1[3] = Uart_read();
	pkt->b1_noisePerMS = readUShort();
	pkt->b1_agcCnt = readUShort();
	pkt->b1_jamInd = Uart_read();
	pkt->b1_ofsI = Uart_read();
	pkt->b1_magI = Uart_read();
	pkt->b1_ofsQ = Uart_read();
	pkt->b1_magQ = Uart_read();
	pkt->b1_reserved2[0] = Uart_read();
	pkt->b1_reserved2[1] = Uart_read();
	pkt->b1_reserved2[2] = Uart_read();
	pkt->b2_blockId = Uart_read();
	pkt->b2_flags = Uart_read();
	pkt->b2_antStatus = Uart_read();
	pkt->b2_antPower = Uart_read();
	pkt->b2_postStatus = readUInt();
	pkt->b2_reserved1[0] = Uart_read();
	pkt->b2_reserved1[1] = Uart_read();
	pkt->b2_reserved1[2] = Uart_read();
	pkt->b2_reserved1[3] = Uart_read();
	pkt->b2_noisePerMS = readUShort();
	pkt->b2_agcCnt = readUShort();
	pkt->b2_jamInd = Uart_read();
	pkt->b2_ofsI = Uart_read();
	pkt->b2_magI = Uart_read();
	pkt->b2_ofsQ = Uart_read();
	pkt->b2_magQ = Uart_read();
	pkt->b2_reserved2[0] = Uart_read();
	pkt->b2_reserved2[1] = Uart_read();
	pkt->b2_reserved2[2] = Uart_read();
}


static void parseACK(ACK * pkt) {
	pkt->clsID = Uart_read();
	pkt->msgID = Uart_read();
}


// functions to read in multi-byte data types
static uint16_t readUShort() {
	uint16_t acc = 0;
	acc |= Uart_read();
	acc |= Uart_read() << 8;
	return acc;
}
static int16_t readShort() {
	int16_t acc = 0;
	acc |= Uart_read();
	acc |= Uart_read() << 8;
	return acc;
}
static uint32_t readUInt() {
	uint32_t acc = 0;
	acc |= Uart_read();
	acc |= Uart_read() << 8;
	acc |= Uart_read() << 16;
	acc |= Uart_read() << 24;
	return acc;
}
static int32_t readInt() {
	int32_t acc = 0;
	acc |= Uart_read();
	acc |= Uart_read() << 8;
	acc |= Uart_read() << 16;
	acc |= Uart_read() << 24;
	return acc;
}

// actually does the checksum
// relies on proper struct packing for the UBX packets
static void doSum(void * data_in, uint8_t * CK_A, uint8_t * CK_B) {
	uint8_t * buf = (uint8_t *) data_in;
	int len = buf[2] + (buf[3]<<8);

	uint8_t a = 0; uint8_t b = 0;

	for (int i = 0; i < len+4; i++) {
		a = a + buf[i];
		b = b + a;
	}

	*CK_A = a;
	*CK_B = b;
}

// digs through the flags to determine if the data is valid
// examines the packet at the current ubxChkTail
static void validateRover() {
	int hpposValid = !(ubxIn.hpposllh.flags & 1);
	int utcFlags = ubxIn.pvt.flags2;
	int utcValid = (utcFlags & 1<<7) && (utcFlags & 1<<6) && (utcFlags & 1<<5);
	int fixValid = ubxIn.pvt.flags & 1;
	int diffValid = ubxIn.pvt.flags & 1<<1;

	bool allValid = fixValid && hpposValid && utcValid;
	ubxIn.dataValid = allValid;

}

