/*
 * ringbuf.c
 *
 *  Created on: Mar 9, 2022
 *      Author: annie
 */

#include "ringbuf.h"
#include <string.h>
#include "ubx.h"

// set up the data ready flag
extern int ring_data_ready = 0;

// pick a uart to attach to the ring buffer
UART_HandleTypeDef huart1;
#define uart &huart1

// declare the buffers and pointers to them
ring_buffer rx_buffer = { { 0 }, 0, 0}; // {buffer}, head, tail
ring_buffer tx_buffer = { { 0 }, 0, 0};
ring_buffer *_rx_buffer;
ring_buffer *_tx_buffer;

// private function definitions
void store_char(unsigned char c, ring_buffer *buffer);

// initialize the ring buffer
void ring_init(void) {
	// initialize the buffer pointers
	_rx_buffer = &rx_buffer;
	_tx_buffer = &tx_buffer;

	// enable interrupts for error and RX register not empty
	//__HAL_UART_ENABLE_IT(uart, UART_IT_ERR);
	__HAL_UART_ENABLE_IT(uart, UART_IT_RXNE);
}

void store_char(unsigned char c, ring_buffer *buffer) {
	// calculate the new head of the buffer
	static int new_head;
	new_head = (unsigned int)(buffer->head + 1) % RING_BUFFER_SIZE;

	// if there's room in the buffer, store the character and advance the head
	if (new_head != buffer->tail) {
		buffer->buffer[buffer->head] = c;
		buffer->head = new_head;
	}
	// else, don't store the character
}

// read a character from the rx_buffer and increment the tail
int Uart_read(void) {
	// declare a static var to avoid mallocing all the time
	static unsigned char c;

	// if the head isn't ahead of the tail, there's nothing in the buffer
	if(_rx_buffer->head == _rx_buffer->tail) {
		return -1;
	} // else retrieve the character and move the tail
	else {
		c = _rx_buffer->buffer[_rx_buffer->tail];
		_rx_buffer->tail = (unsigned int)(_rx_buffer->tail + 1) % RING_BUFFER_SIZE;
		return c;
	}

}

// write a character into the tx_buffer and increment the head
void Uart_write(int c) {
	static int i;

	if (c>=0) {
		// retrieve the next head of the buffer
		i = (_tx_buffer->head + 1) % RING_BUFFER_SIZE;

		// note: there is no overrun error checking. this is probably fine.
		// the tx_buffer will be overwritten, causing data corruption, but
		// if you got an overrun error you were getting corrupt data anyway :)

		// ok now put the character into the damn buffer
		_tx_buffer->buffer[_tx_buffer->head] = (uint8_t)c;
		_tx_buffer->head = i; // and update the head

		// enable the UART transmission interrupt to actually send the data
		__HAL_UART_ENABLE_IT(uart, UART_IT_TXE);
	}
}

/* Checks if the data is available to read in the rx_buffer */
int IsDataAvailable(void) {
	return (uint16_t)(RING_BUFFER_SIZE + _rx_buffer->head - _rx_buffer->tail) % RING_BUFFER_SIZE;
}

// ISR to move data from uart registers into my buffer
void ring_uart_isr(UART_HandleTypeDef *huart) {
	// avoid dynamic allocation from within an ISR
	static uint32_t isrflags;
	static uint32_t cr1its;
	static unsigned char c;

	// read the registers
	isrflags = READ_REG(huart->Instance->ISR);
	cr1its   = READ_REG(huart->Instance->CR1);

	// handle RXNE by storing a character
	if (((isrflags & USART_ISR_RXNE) != RESET) && ((cr1its & USART_CR1_RXNEIE) != RESET)) {
		// read the status register (this time to reset flags)
		huart->Instance->ISR;

		// read the character in
		c = huart->Instance->RDR;

		// store the data in the buffer
		store_char(c, _rx_buffer);

		// might be wrong, but: turn the RXNE interrupt back on
		//__HAL_UART_ENABLE_IT(huart, UART_IT_RXNE);

		return;
	}

	// handle TXE by outputting a character
    if (((isrflags & USART_ISR_TXE) != RESET) && ((cr1its & USART_CR1_TXEIE) != RESET)) {
    	// check if the buffer is empty
    	if (tx_buffer.head == tx_buffer.tail) {
    		// buffer is empty, so disable the transmit interrupt
    		__HAL_UART_DISABLE_IT(huart, UART_IT_TXE);
    	} else {
    		// there's more data in the buffer. grab the next byte and increment the tail
    		c = tx_buffer.buffer[tx_buffer.tail];
  	        tx_buffer.tail = (tx_buffer.tail + 1) % RING_BUFFER_SIZE;

  	        // clear the flags by reading the status register
  	        huart->Instance->ISR;

  	        // now actually send the character by putting it in the outbound register
  	        huart->Instance->TDR = c;

  	        return;
    	}
    }

	// handle IDLE by setting the packet ready flag
    if ((isrflags & USART_ISR_IDLE)) {
    	// clear the status and data registers (not sure if data reg needs to be cleared)
    	huart->Instance->ISR;
    	huart->Instance->RDR;

    	// clear the idle flag
    	huart->Instance->ICR |= USART_ICR_IDLECF;

    	// set a flag to 1 (to do!)
    	ring_data_ready = 1;
    	FLAG_UBX_READY = 1;
    	return;
    }
    // if you're here, something else has gone wrong.
    // read the ISR register to try to reset the flags?
	huart->Instance->ISR;
}
