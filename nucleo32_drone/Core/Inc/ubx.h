/*
 * ubx.h
 *
 *  Created on: Jun 26, 2020
 *      Author: annie
 */

#ifndef INC_UBX_H_
#define INC_UBX_H_

#include <stdint.h>
#include <stdbool.h>
#include <main.h>

uint8_t FLAG_UBX_READY;    // a UBX packet is waiting on the UART buffer
uint8_t FLAG_UBX_PARSED; // a UBX packet has arrived and is in ubxIn
uint8_t FLAG_STRING_READY; // a UBX packet has been converted to a string and is ready to write to SD card

uint8_t FLAG_SURVEY_STATUS;

// public variables for accessing updated UBX data
extern uint32_t UBX_thisTick; // time of this UBX packet on the internal clock
extern uint32_t UBX_lastTick; // time of last UBX packet on the internal clock
extern uint8_t UBX_diffStatus; // status of differential corrections
extern uint32_t UBX_carrierStatus; // status of carrier phase range corrections
extern uint32_t UBX_time; // UTC time
extern uint32_t UBX_iTOW; // GPS time of week (down to ms)
extern bool UBX_locked; // is the location locked/does the GPS have a time-only fix?
extern int UBX_lat; // current latitude in deg * 10^-7
extern int UBX_lon; // current longitude in deg * 10^-7
extern uint8_t UBX_numSV; // number of satellites visible
extern bool UBX_svinActive; // is the survey-in procedure running?
extern uint32_t UBX_svinProg; // number of seconds the survey-in has been running
extern uint32_t UBX_svinAcc; // accuracy of the survey-in procedure
extern uint32_t UBX_hAcc; // horizontal accuracy estimate in 1/10th mm
extern uint32_t UBX_fullyResolved; // is the UTC time resolved without full-second ambiguity?

/*UBX sync chars*/
#define UBX_SYN_CHAR1    0xB5
#define UBX_SYN_CHAR2    0x62

/*UBX SM states*/
#define  SM_UBX_BEFORE      0
#define  SM_UBX_SYN2    	1
#define  SM_UBX_CLASS     	2
#define  SM_UBX_ID    		3
#define  SM_UBX_PAYLEN1  	4
#define  SM_UBX_PAYLEN2   	5
#define  SM_UBX_PAYLOAD     6
#define  SM_UBX_CHK1      	7
#define  SM_UBX_CHK2      	8
#define  SM_UBX_ERR         9
#define  SM_UBX_END        10

// config stuff
#define MAX_CFG_PAIRS 32

enum
{
	// UBX CLASS ID
	UBX_NAV     	 	   = 0x01   //Navigation Results Messages
	,UBX_ACK               = 0x05
	,UBX_CFG               = 0x06
	,UBX_MON               = 0x0a
	// UBX MESSAGE ID
	,UBX_NAV_PVT         = 0x07   //NAV-PVT: Navigation Position Velocity Time Solution
	,UBX_NAV_HPPOSLLH    = 0x14   //NAV-HPPOSLLH: High precision coordinates
	,UBX_NAV_STATUS      = 0x03   //NAV-STATUS: A big pile of status flags
	,UBX_NAV_SVIN        = 0x3b   //NAV-SVIN: The status of the current survey-in
	,UBX_MON_RF          = 0x38   //MON-RF: Antenna RF monitoring data
	,UBX_ACK_ACK         = 0x01   //ACK-ACK: Affirmative acknowledgment
	,UBX_ACK_NAK         = 0x00   //ACK-NAK: Negative acknowledgment
	,UBX_CFG_VALSET      = 0x8a   //CFG-VALSET: Changes settings on the GPS (tx only)
};

// CFG Packet IDs
enum {
	// TMODE, for Survey-In or Fixed Position
	CFG_TMODE_MODE          = 0x20030001 //[E1] 0:disabled, 1:survey, 2:fixed
	,CFG_TMODE_SVIN_MIN_DUR = 0x40030010 //[U4] survey duration in seconds
	,CFG_TMODE_SVIN_ACC_LIM = 0x40030011 //[U4] survey accuracy in 0.1mm
	,CFG_TMODE_LON          = 0x4003000a //[L4] fixed pos lon
	,CFG_TMODE_LAT          = 0x40030009 //[L4] fixed pos lat
	,CFG_TMODE_HEIGHT       = 0x4003000b //[L4] fixed pos height (cm)
	,CFG_TMODE_POS_TYPE     = 0x20030002 //[E1] set to 1 for LLH

	// dynamic model, UTC, and RTK settings
	,CFG_NAVHPG_DGNSSMODE   = 0x20140011 //[E1] set to 3 to use RTK
	,CFG_NAVSPG_DYNMODEL    = 0x20110021 //[E1] 0:port, 2:stationary, 7:airborne,<2g
	,CFG_NAVSPG_UTCSTANDARD = 0x2011001c //[E1] set to 3 for USNO/GPS

	// enable or disable constellations
	// all binary flags
	,CFG_SIGNAL_SBAS_ENA = 0x10310020 //[L]
	,CFG_SIGNAL_BDS_ENA  = 0x10310022 //[L]
	,CFG_SIGNAL_GAL_ENA  = 0x10310021 //[L]
	,CFG_SIGNAL_QZSS_ENA = 0x10310024 //[L]
	,CFG_SIGNAL_GLO_ENA  = 0x10310025 //[L]

	// rate settings
	,CFG_RATE_MEAS = 0x30210001 //[U2] time between measurements in ms
	,CFG_RATE_NAV  = 0x30210002 //[U2] number of observations per measurement

	// timepulse
	,CFG_TP_PULSE_DEF        = 0x20050023 //[E1] 0:period, 1:frequency
	,CFG_TP_PULSE_LENGTH_DEF = 0x20050030 //[E1] 0:ratio, 1:length
	,CFG_TP_PERIOD_TP1       = 0x40050002 //[U4] seconds * 10^-6
	,CFG_TP_FREQ_TP1         = 0x40050024 //[U4] hz
	,CFG_TP_LEN_TP1          = 0x40050004 //[U4] seconds * 10^-6
	,CFG_TP_DUTY_TP1         = 0x5005002a //[R8] floating point duty cycle (out of 100)
	,CFG_TP_TP1_ENA          = 0x10050007 //[L] enables the timepulse
	,CFG_TP_POL_TP1          = 0x1005000b //[L] 0:falling edge at top of second, 1:rising
	,CFG_TP_USE_LOCKED_TP1   = 0x10050009 //[L] MUST be set to 0 for correct TP behavior

	// interface options and protocols
	,CFG_UART1_BAUDRATE     = 0x40520001 //[U4]
	,CFG_UART1INPROT_UBX    = 0x10730001 //[L]
	,CFG_UART1INPROT_NMEA   = 0x10730002 //[L]
	,CFG_UART1INPROT_RTCM3X = 0x10730004 //[L]
	,CFG_UART1OUTPROT_UBX   = 0x10740001 //[L]
	,CFG_UART1OUTPROT_NMEA  = 0x10740002 //[L]
	,CFG_UART1OUTPROT_RTCM3X= 0x10740004 //[L]

	,CFG_UART2_BAUDRATE     = 0x40530001 //[U4]
	,CFG_UART2INPROT_UBX    = 0x10750001 //[L]
	,CFG_UART2INPROT_NMEA   = 0x10750002 //[L]
	,CFG_UART2INPROT_RTCM3X = 0x10750004 //[L]
	,CFG_UART2OUTPROT_UBX   = 0x10760001 //[L]
	,CFG_UART2OUTPROT_NMEA  = 0x10760002 //[L]
	,CFG_UART2OUTPROT_RTCM3X= 0x10760004 //[L]

	// messages (CFG-MSGOUT)

	,CFG_MSGOUT_NMEA_ID_DTM_UART2   = 0x209100a8 //[U1]
	,CFG_MSGOUT_NMEA_ID_GBS_UART2   = 0x209100df //[U1]
	,CFG_MSGOUT_NMEA_ID_GGA_UART2   = 0x209100bc //[U1]
	,CFG_MSGOUT_NMEA_ID_GLL_UART2   = 0x209100cb //[U1]
	,CFG_MSGOUT_NMEA_ID_GNS_UART2   = 0x209100b7 //[U1]
	,CFG_MSGOUT_NMEA_ID_GRS_UART2   = 0x209100d0 //[U1]
	,CFG_MSGOUT_NMEA_ID_GSA_UART2   = 0x209100c1 //[U1]
	,CFG_MSGOUT_NMEA_ID_GST_UART2   = 0x209100d5 //[U1]
	,CFG_MSGOUT_NMEA_ID_GSV_UART2   = 0x209100c6 //[U1]
	,CFG_MSGOUT_NMEA_ID_RMC_UART2   = 0x209100ad //[U1]
	,CFG_MSGOUT_NMEA_ID_VLW_UART2   = 0x209100e9 //[U1]
	,CFG_MSGOUT_NMEA_ID_VTG_UART2   = 0x209100b2 //[U1]
	,CFG_MSGOUT_NMEA_ID_ZDA_UART2   = 0x209100da //[U1]
	,CFG_MSGOUT_PUBX_ID_POLYP_UART2 = 0x209100ee //[U1]
	,CFG_MSGOUT_PUBX_ID_POLYS_UART2 = 0x209100f3 //[U1]
	,CFG_MSGOUT_PUBX_ID_POLYT_UART2 = 0x209100f8 //[U1]

	,CFG_MSGOUT_RTCM_3X_TYPE1005_UART2 = 0x209102bf //[U1]
	,CFG_MSGOUT_RTCM_3X_TYPE1074_UART2 = 0x20910360 //[U1]
	,CFG_MSGOUT_RTCM_3X_TYPE1084_UART2 = 0x20910365 //[U1]
	,CFG_MSGOUT_RTCM_3X_TYPE1094_UART2 = 0x2091036a //[U1]
	,CFG_MSGOUT_RTCM_3X_TYPE1230_UART2 = 0x20910305 //[U1]

	,CFG_MSGOUT_UBX_NAV_HPPOSLLH_UART1 = 0x20910034 //[U1]
	,CFG_MSGOUT_UBX_NAV_PVT_UART1      = 0x20910007 //[U1]
	,CFG_MSGOUT_UBX_NAV_STATUS_UART1   = 0x2091001b //[U1]
	,CFG_MSGOUT_UBX_NAV_SVIN_UART1     = 0x20910089 //[U1]
	,CFG_MSGOUT_UBX_MON_RF_UART1       = 0x2091035a //[U1]

};

// uart handles for use with UartRingBuffer
extern UART_HandleTypeDef huart1;

// structs for individual packets
typedef struct {
	uint8_t class;
	uint8_t ID;
	uint16_t length;
	uint32_t iTOW;
	uint16_t year;
	uint8_t month;
	uint8_t day;
	uint8_t hour;
	uint8_t min;
	uint8_t sec;
	uint8_t valid; //flags
	uint32_t tAcc;
	int32_t nano;
	uint8_t fixType;
	uint8_t flags;
	uint8_t flags2;
	uint8_t numSV;
	int32_t lon;
	int32_t lat;
	int32_t height;
	int32_t hMSL;
	uint32_t hAcc;
	uint32_t vAcc;
	int32_t velN;
	int32_t velE;
	int32_t velD;
	int32_t gSpeed;
	int32_t headMot;
	uint32_t sAcc;
	uint32_t headAcc;
	uint16_t pDOP;
	uint8_t flags3;
	uint8_t reserved[5];
	int32_t headVeh;
	int16_t magDec;
	uint16_t macAcc;
	uint8_t CK_A;
	uint8_t CK_B;
} __attribute__((packed)) PVT;

typedef struct {
	uint8_t class;
	uint8_t ID;
	uint16_t length;
	uint8_t version;
	uint16_t reserved;
	uint8_t flags;
	uint32_t iTOW;
	int32_t lon;
	int32_t lat;
	int32_t height;
	int32_t hMSL;
	int8_t lonHp;
	int8_t latHp;
	int8_t heightHp;
	int8_t hMSLHp;
	uint32_t hAcc;
	uint32_t vAcc;
	uint8_t CK_A;
	uint8_t CK_B;
} __attribute__((packed)) HPPOSLLH;

typedef struct {
	uint8_t class;
	uint8_t ID;
	uint16_t length;
	uint32_t iTOW;
	uint8_t gpsFix;
	uint8_t flags;
	uint8_t fixStat;
	uint8_t flags2;
	uint32_t ttff;
	uint32_t msss;
	uint8_t CK_A;
	uint8_t CK_B;
} __attribute__((packed)) STATUS;

typedef struct {
	uint8_t class;
	uint8_t ID;
	uint16_t length;
	uint8_t version;
	uint8_t reserved[3];
	uint32_t iTOW;
	uint32_t dur;
	int32_t meanX;
	int32_t meanY;
	int32_t meanZ;
	int8_t meanXHP;
	int8_t meanYHP;
	int8_t meanZHP;
	uint8_t reserved1;
	uint32_t meanAcc;
	uint32_t obs;
	uint8_t valid;
	uint8_t active;
	uint16_t reserved2;
	uint8_t CK_A;
	uint8_t CK_B;
} __attribute__((packed)) SVIN;

typedef struct {
	uint8_t class;
	uint8_t ID;
	uint16_t length;
	uint8_t version;
	uint8_t nBlocks; // should be 2, otherwise this structure breaks
	uint8_t reserved[2];
	uint8_t b1_blockId;
	uint8_t b1_flags;
	uint8_t b1_antStatus;
	uint8_t b1_antPower;
	uint32_t b1_postStatus;
	uint8_t b1_reserved1[4];
	uint16_t b1_noisePerMS;
	uint16_t b1_agcCnt;
	uint8_t b1_jamInd;
	int8_t b1_ofsI;
	uint8_t b1_magI;
	int8_t b1_ofsQ;
	uint8_t b1_magQ;
	uint8_t b1_reserved2[3];
	uint8_t b2_blockId;
	uint8_t b2_flags;
	uint8_t b2_antStatus;
	uint8_t b2_antPower;
	uint32_t b2_postStatus;
	uint8_t b2_reserved1[4];
	uint16_t b2_noisePerMS;
	uint16_t b2_agcCnt;
	uint8_t b2_jamInd;
	int8_t b2_ofsI;
	uint8_t b2_magI;
	int8_t b2_ofsQ;
	uint8_t b2_magQ;
	uint8_t b2_reserved2[3];
	uint8_t CK_A;
	uint8_t CK_B;
} __attribute__((packed)) RF;

typedef struct {
	uint8_t class;
	uint8_t ID;
	uint16_t length;
	uint8_t clsID;
	uint8_t msgID;
	uint8_t CK_A;
	uint8_t CK_B;
} __attribute((packed)) ACK;

// One of each of the packets, plus strings and checksum flags
typedef struct {
	// the string that will be stored in the CSV file
	char CSVString[400];

	// storage for individual packets
	PVT pvt;
	HPPOSLLH hpposllh;
	SVIN svin;
	RF rf;

	// is the checksum valid?
	bool chkValid;
	// is the data valid?
	bool dataValid;
} ubxLine;

// now without a ring buffer
ubxLine ubxIn; // the actual UBX data
bool UBX_validated; // has this packet been verified?
// FLAG_UBX_READY monitors data reading
// FLAG_STRING_READY monitors data logging

// function prototypes

// the idle line ISR function
void UBXIdle();

// checks the checksums of the next packet bundle
void UBXCheckPackets();

// returns true if and only if the next packet contains good data
bool UBXIsNextValid();

// constructs the CSV string from the next available dataLine in the buffer
// returns 0 if successful, else something else
int buildString();

// copies the next string to write into the outBuf and increments the index
char * getString();

// gets the timepulse configuration string
char * getTPString();

// adds a cfgval to the building cfg_buf
bool cfg_add(uint32_t key, uint8_t * value, uint8_t size);

// clears the cfg_buf
void cfg_clear();

// sends the cfg buffer
void cfg_transmit();

// sends a timepulse config packet
void sendTPConfig(uint32_t period, uint32_t len);

// sends the default config packets
void sendBaseConfig();
void sendRoverConfig();

// turns rover telemetry feed on or off
void sendTelemConfig(bool setting);

#endif /* INC_UBX_H_ */
