/*
 * neopix.h
 *
 *  Created on: Mar 8, 2022
 *      Author: annie
 *
 *
 *      WARNING: bitbanging ahead!
 *      This module REQUIRES a SPI data rate of exactly 3MBits/s,
 *      which requires a peripheral clock speed that is a multiple of 3MHz
 *
 *      If your LEDs don't work, check your clock speeds.
 *
 */

#ifndef INC_NEOPIX_H_
#define INC_NEOPIX_H_

#include "stm32l4xx_hal.h"

// defines the number of LEDs in the string
#define LED_NO    3

// defines the size of the DMA output buffer
#define LED_BUFFER_LENGTH (LED_NO*36)

// LED mode definitions
#define LED_MODE_SOLID 0
#define LED_MODE_BLINK 1
#define LED_MODE_FADE  2

void initLEDMOSI();
void setPixelColor(uint16_t n, uint8_t r, uint8_t g, uint8_t b);
void setAllPixelColor(uint8_t r, uint8_t g, uint8_t b);
void setPixelHSV(uint16_t n, uint16_t hue, uint8_t sat, uint8_t val);
void rainbowDelay(uint32_t millis);

// set the LED color
void LED_setColor(uint8_t r, uint8_t g, uint8_t b);
// change the mode
void LED_setMode(uint8_t modeIn);
// change the rate
void LED_setRate(uint32_t rate);
// the main refresh task
void LED_refreshTask();

#endif /* INC_NEOPIX_H_ */
