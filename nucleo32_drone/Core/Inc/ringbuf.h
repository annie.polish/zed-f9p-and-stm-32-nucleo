/*
 * ringbuf.h
 *
 *  Created on: Mar 9, 2022
 *      Author: annie
 */

#ifndef INC_RINGBUF_H_
#define INC_RINGBUF_H_

#include "stm32l4xx_hal.h"

int ring_data_ready;

// define the size of the buffer
#define RING_BUFFER_SIZE 512

// define the buffer struct
typedef struct
{
  unsigned char buffer[RING_BUFFER_SIZE];
  volatile unsigned int head;
  volatile unsigned int tail;
} ring_buffer;

// initialize the ring buffer
void ring_init(void);

// TODO: Change naming convention!

// read a character into the rx_buffer and increment the tail
int Uart_read(void);

// write a character into the tx_buffer and increment the tail
void Uart_write(int c);

// check if data is available on the rx_buffer
int IsDataAvailable(void);

// ISR to move data from uart registers into my buffer
void ring_uart_isr(UART_HandleTypeDef *huart);


#endif /* INC_RINGBUF_H_ */
