/*
 * BNO055.h
 *
 *  Created on: Mar 13, 2021
 *      Author: annie
 *
 *  Ported from the Adafruit Arduino drivers
 *  https://github.com/adafruit/Adafruit_BNO055
 */

#ifndef INC_BNO055_H_
#define INC_BNO055_H_

#include <stdbool.h>

// Public storage variables
// Euler angles
extern double IMU_pitchf;
extern double IMU_rollf;
extern double IMU_yawf;
extern int32_t IMU_pitch;
extern int32_t IMU_roll;
extern int32_t IMU_yaw;
// Quaternions
extern double IMU_qw;
extern double IMU_qx;
extern double IMU_qy;
extern double IMU_qz;

// start the BNO055 in IMU mode
bool IMU_begin();

// get an IMU reading in Euler angles and put it into the public storage vars
void IMU_getEuler();

// get an IMU reading as a Quaternion and put it into the public storage vars
void IMU_getQuat();




#endif /* INC_BNO055_H_ */
