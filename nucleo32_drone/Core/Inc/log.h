/*
 * log.h
 *
 *  Created on: Aug 29, 2020
 *      Author: annie
 */

#ifndef INC_LOG_H_
#define INC_LOG_H_

#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "main.h"
#include "ubx.h"

// has the logName been set?
extern bool logNameSet;

// has the dataName been set?
extern bool dataNameSet;

// set the name of the log file
void log_setName(char * fname);

// logs a line to the log file
void log_writeLine(char * lineIn);

// sends a sync command and halts logging
void log_sendSync();

void roverLoggerTask();

#endif /* INC_LOG_H_ */
