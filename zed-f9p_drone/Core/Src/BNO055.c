/*
 * BNO055.c
 *
 *  Created on: Mar 13, 2021
 *      Author: annie
 *
 *  Ported from the Adafruit Arduino drivers
 *  https://github.com/adafruit/Adafruit_BNO055
 */

#include "stm32l1xx_hal.h"
#include <ctype.h>
#include <stdint.h>
#include "BNO055.h"

I2C_HandleTypeDef hi2c1;

// public vars for polled data
double IMU_pitchf = 0;
double IMU_rollf = 0;
double IMU_yawf = 0;

int32_t IMU_pitch = 0;
int32_t IMU_roll = 0;
int32_t IMU_yaw = 0;

double IMU_qw = 0;
double IMU_qx = 0;
double IMU_qy = 0;
double IMU_qz = 0;

/** BNO055 Address A **/
#define BNO055_ADDRESS_A (0x28)
/** BNO055 Address B **/
#define BNO055_ADDRESS_B (0x29)
/** BNO055 ID **/
#define BNO055_ID (0xA0)

#define BNO055_ADDR BNO055_ADDRESS_A

/** Offsets registers **/
#define NUM_BNO055_OFFSET_REGISTERS (22)

/** BNO055 Registers **/
typedef enum {
	/* Page id register definition */
	BNO055_PAGE_ID_ADDR = 0X07,

	/* PAGE0 REGISTER DEFINITION START*/
	BNO055_CHIP_ID_ADDR = 0x00,
	BNO055_ACCEL_REV_ID_ADDR = 0x01,
	BNO055_MAG_REV_ID_ADDR = 0x02,
	BNO055_GYRO_REV_ID_ADDR = 0x03,
	BNO055_SW_REV_ID_LSB_ADDR = 0x04,
	BNO055_SW_REV_ID_MSB_ADDR = 0x05,
	BNO055_BL_REV_ID_ADDR = 0X06,

	/* Accel data register */
	BNO055_ACCEL_DATA_X_LSB_ADDR = 0X08,
	BNO055_ACCEL_DATA_X_MSB_ADDR = 0X09,
	BNO055_ACCEL_DATA_Y_LSB_ADDR = 0X0A,
	BNO055_ACCEL_DATA_Y_MSB_ADDR = 0X0B,
	BNO055_ACCEL_DATA_Z_LSB_ADDR = 0X0C,
	BNO055_ACCEL_DATA_Z_MSB_ADDR = 0X0D,

	/* Mag data register */
	BNO055_MAG_DATA_X_LSB_ADDR = 0X0E,
	BNO055_MAG_DATA_X_MSB_ADDR = 0X0F,
	BNO055_MAG_DATA_Y_LSB_ADDR = 0X10,
	BNO055_MAG_DATA_Y_MSB_ADDR = 0X11,
	BNO055_MAG_DATA_Z_LSB_ADDR = 0X12,
	BNO055_MAG_DATA_Z_MSB_ADDR = 0X13,

	/* Gyro data registers */
	BNO055_GYRO_DATA_X_LSB_ADDR = 0X14,
	BNO055_GYRO_DATA_X_MSB_ADDR = 0X15,
	BNO055_GYRO_DATA_Y_LSB_ADDR = 0X16,
	BNO055_GYRO_DATA_Y_MSB_ADDR = 0X17,
	BNO055_GYRO_DATA_Z_LSB_ADDR = 0X18,
	BNO055_GYRO_DATA_Z_MSB_ADDR = 0X19,

	/* Euler data registers */
	BNO055_EULER_H_LSB_ADDR = 0X1A,
	BNO055_EULER_H_MSB_ADDR = 0X1B,
	BNO055_EULER_R_LSB_ADDR = 0X1C,
	BNO055_EULER_R_MSB_ADDR = 0X1D,
	BNO055_EULER_P_LSB_ADDR = 0X1E,
	BNO055_EULER_P_MSB_ADDR = 0X1F,

	/* Quaternion data registers */
	BNO055_QUATERNION_DATA_W_LSB_ADDR = 0X20,
	BNO055_QUATERNION_DATA_W_MSB_ADDR = 0X21,
	BNO055_QUATERNION_DATA_X_LSB_ADDR = 0X22,
	BNO055_QUATERNION_DATA_X_MSB_ADDR = 0X23,
	BNO055_QUATERNION_DATA_Y_LSB_ADDR = 0X24,
	BNO055_QUATERNION_DATA_Y_MSB_ADDR = 0X25,
	BNO055_QUATERNION_DATA_Z_LSB_ADDR = 0X26,
	BNO055_QUATERNION_DATA_Z_MSB_ADDR = 0X27,

	/* Linear acceleration data registers */
	BNO055_LINEAR_ACCEL_DATA_X_LSB_ADDR = 0X28,
	BNO055_LINEAR_ACCEL_DATA_X_MSB_ADDR = 0X29,
	BNO055_LINEAR_ACCEL_DATA_Y_LSB_ADDR = 0X2A,
	BNO055_LINEAR_ACCEL_DATA_Y_MSB_ADDR = 0X2B,
	BNO055_LINEAR_ACCEL_DATA_Z_LSB_ADDR = 0X2C,
	BNO055_LINEAR_ACCEL_DATA_Z_MSB_ADDR = 0X2D,

	/* Gravity data registers */
	BNO055_GRAVITY_DATA_X_LSB_ADDR = 0X2E,
	BNO055_GRAVITY_DATA_X_MSB_ADDR = 0X2F,
	BNO055_GRAVITY_DATA_Y_LSB_ADDR = 0X30,
	BNO055_GRAVITY_DATA_Y_MSB_ADDR = 0X31,
	BNO055_GRAVITY_DATA_Z_LSB_ADDR = 0X32,
	BNO055_GRAVITY_DATA_Z_MSB_ADDR = 0X33,

	/* Temperature data register */
	BNO055_TEMP_ADDR = 0X34,

	/* Status registers */
	BNO055_CALIB_STAT_ADDR = 0X35,
	BNO055_SELFTEST_RESULT_ADDR = 0X36,
	BNO055_INTR_STAT_ADDR = 0X37,

	BNO055_SYS_CLK_STAT_ADDR = 0X38,
	BNO055_SYS_STAT_ADDR = 0X39,
	BNO055_SYS_ERR_ADDR = 0X3A,

	/* Unit selection register */
	BNO055_UNIT_SEL_ADDR = 0X3B,
	BNO055_DATA_SELECT_ADDR = 0X3C,

	/* Mode registers */
	BNO055_OPR_MODE_ADDR = 0X3D,
	BNO055_PWR_MODE_ADDR = 0X3E,

	BNO055_SYS_TRIGGER_ADDR = 0X3F,
	BNO055_TEMP_SOURCE_ADDR = 0X40,

	/* Axis remap registers */
	BNO055_AXIS_MAP_CONFIG_ADDR = 0X41,
	BNO055_AXIS_MAP_SIGN_ADDR = 0X42,

	/* SIC registers */
	BNO055_SIC_MATRIX_0_LSB_ADDR = 0X43,
	BNO055_SIC_MATRIX_0_MSB_ADDR = 0X44,
	BNO055_SIC_MATRIX_1_LSB_ADDR = 0X45,
	BNO055_SIC_MATRIX_1_MSB_ADDR = 0X46,
	BNO055_SIC_MATRIX_2_LSB_ADDR = 0X47,
	BNO055_SIC_MATRIX_2_MSB_ADDR = 0X48,
	BNO055_SIC_MATRIX_3_LSB_ADDR = 0X49,
	BNO055_SIC_MATRIX_3_MSB_ADDR = 0X4A,
	BNO055_SIC_MATRIX_4_LSB_ADDR = 0X4B,
	BNO055_SIC_MATRIX_4_MSB_ADDR = 0X4C,
	BNO055_SIC_MATRIX_5_LSB_ADDR = 0X4D,
	BNO055_SIC_MATRIX_5_MSB_ADDR = 0X4E,
	BNO055_SIC_MATRIX_6_LSB_ADDR = 0X4F,
	BNO055_SIC_MATRIX_6_MSB_ADDR = 0X50,
	BNO055_SIC_MATRIX_7_LSB_ADDR = 0X51,
	BNO055_SIC_MATRIX_7_MSB_ADDR = 0X52,
	BNO055_SIC_MATRIX_8_LSB_ADDR = 0X53,
	BNO055_SIC_MATRIX_8_MSB_ADDR = 0X54,

	/* Accelerometer Offset registers */
	ACCEL_OFFSET_X_LSB_ADDR = 0X55,
	ACCEL_OFFSET_X_MSB_ADDR = 0X56,
	ACCEL_OFFSET_Y_LSB_ADDR = 0X57,
	ACCEL_OFFSET_Y_MSB_ADDR = 0X58,
	ACCEL_OFFSET_Z_LSB_ADDR = 0X59,
	ACCEL_OFFSET_Z_MSB_ADDR = 0X5A,

	/* Magnetometer Offset registers */
	MAG_OFFSET_X_LSB_ADDR = 0X5B,
	MAG_OFFSET_X_MSB_ADDR = 0X5C,
	MAG_OFFSET_Y_LSB_ADDR = 0X5D,
	MAG_OFFSET_Y_MSB_ADDR = 0X5E,
	MAG_OFFSET_Z_LSB_ADDR = 0X5F,
	MAG_OFFSET_Z_MSB_ADDR = 0X60,

	/* Gyroscope Offset register s*/
	GYRO_OFFSET_X_LSB_ADDR = 0X61,
	GYRO_OFFSET_X_MSB_ADDR = 0X62,
	GYRO_OFFSET_Y_LSB_ADDR = 0X63,
	GYRO_OFFSET_Y_MSB_ADDR = 0X64,
	GYRO_OFFSET_Z_LSB_ADDR = 0X65,
	GYRO_OFFSET_Z_MSB_ADDR = 0X66,

	/* Radius registers */
	ACCEL_RADIUS_LSB_ADDR = 0X67,
	ACCEL_RADIUS_MSB_ADDR = 0X68,
	MAG_RADIUS_LSB_ADDR = 0X69,
	MAG_RADIUS_MSB_ADDR = 0X6A
} adafruit_bno055_reg_t;

/** BNO055 power settings */
typedef enum {
	POWER_MODE_NORMAL = 0X00,
	POWER_MODE_LOWPOWER = 0X01,
	POWER_MODE_SUSPEND = 0X02
} adafruit_bno055_powermode_t;

/** Operation mode settings **/
typedef enum {
	OPERATION_MODE_CONFIG = 0X00,
	OPERATION_MODE_ACCONLY = 0X01,
	OPERATION_MODE_MAGONLY = 0X02,
	OPERATION_MODE_GYRONLY = 0X03,
	OPERATION_MODE_ACCMAG = 0X04,
	OPERATION_MODE_ACCGYRO = 0X05,
	OPERATION_MODE_MAGGYRO = 0X06,
	OPERATION_MODE_AMG = 0X07,
	OPERATION_MODE_IMUPLUS = 0X08,
	OPERATION_MODE_COMPASS = 0X09,
	OPERATION_MODE_M4G = 0X0A,
	OPERATION_MODE_NDOF_FMC_OFF = 0X0B,
	OPERATION_MODE_NDOF = 0X0C
} adafruit_bno055_opmode_t;

/** Remap settings **/
typedef enum {
	REMAP_CONFIG_P0 = 0x21,
	REMAP_CONFIG_P1 = 0x24, // default
	REMAP_CONFIG_P2 = 0x24,
	REMAP_CONFIG_P3 = 0x21,
	REMAP_CONFIG_P4 = 0x24,
	REMAP_CONFIG_P5 = 0x21,
	REMAP_CONFIG_P6 = 0x21,
	REMAP_CONFIG_P7 = 0x24
} adafruit_bno055_axis_remap_config_t;

/** Remap Signs **/
typedef enum {
	REMAP_SIGN_P0 = 0x04,
	REMAP_SIGN_P1 = 0x00, // default
	REMAP_SIGN_P2 = 0x06,
	REMAP_SIGN_P3 = 0x02,
	REMAP_SIGN_P4 = 0x03,
	REMAP_SIGN_P5 = 0x01,
	REMAP_SIGN_P6 = 0x07,
	REMAP_SIGN_P7 = 0x05
} adafruit_bno055_axis_remap_sign_t;

/** A structure to represent revisions **/
typedef struct {
	uint8_t accel_rev; /**< acceleration rev */
	uint8_t mag_rev;   /**< magnetometer rev */
	uint8_t gyro_rev;  /**< gyroscrope rev */
	uint16_t sw_rev;   /**< SW rev */
	uint8_t bl_rev;    /**< bootloader rev */
} adafruit_bno055_rev_info_t;

/** Vector Mappings **/
typedef enum {
	VECTOR_ACCELEROMETER = BNO055_ACCEL_DATA_X_LSB_ADDR,
	VECTOR_MAGNETOMETER = BNO055_MAG_DATA_X_LSB_ADDR,
	VECTOR_GYROSCOPE = BNO055_GYRO_DATA_X_LSB_ADDR,
	VECTOR_EULER = BNO055_EULER_H_LSB_ADDR,
	VECTOR_LINEARACCEL = BNO055_LINEAR_ACCEL_DATA_X_LSB_ADDR,
	VECTOR_GRAVITY = BNO055_GRAVITY_DATA_X_LSB_ADDR
} adafruit_vector_type_t;

// PRIVATE FUNCTIONS
// send 1 byte of data
void sendByte(uint8_t data) {
	uint8_t data_out = data;
	HAL_I2C_Master_Transmit(&hi2c1, BNO055_ADDR<<1, &data_out, 1, 100);
}

// read a byte from a given register
uint8_t read8(adafruit_bno055_reg_t reg) {
	uint8_t _reg = reg;
	uint8_t retval = 0;
	HAL_I2C_Mem_Read(&hi2c1, BNO055_ADDR<<1, _reg, 0xFF, &retval, 1, 100);
	return retval;
}

// write a byte to a given register
void write8(adafruit_bno055_reg_t reg, uint8_t val) {
	uint16_t datOut = (val<<8) + reg;
	HAL_I2C_Master_Transmit(&hi2c1, BNO055_ADDR<<1, &datOut, 2, 100);
}

// PUBLIC FUNCTIONS

// start the BNO055 in IMU mode
bool IMU_begin() {
	// poll the sensor and check that it's responding correctly
	uint8_t id = read8(BNO055_CHIP_ID_ADDR);
	if (id != BNO055_ID) {
		HAL_Delay(500); // wait and try again
		id = read8(BNO055_CHIP_ID_ADDR);
		if (id != BNO055_ID) return false;
	}

	// switch to config mode (just in case)
	HAL_Delay(30);
	write8(BNO055_OPR_MODE_ADDR, OPERATION_MODE_CONFIG);

	// reset the chip and wait for it to come back up
	HAL_Delay(30);
	write8(BNO055_SYS_TRIGGER_ADDR, 0x20);
	/* Delay increased to 30ms due to power issues https://tinyurl.com/y375z699 */
	HAL_Delay(30);
	while (read8(BNO055_CHIP_ID_ADDR) != BNO055_ID) HAL_Delay(10);
	HAL_Delay(50);

	// set to normal power mode
	write8(BNO055_PWR_MODE_ADDR, POWER_MODE_NORMAL);
	HAL_Delay(30);

	// go to page 0 of the registers to change some more settings
	write8(BNO055_PAGE_ID_ADDR, 0);
	HAL_Delay(500);

	// set the triggering to the external crystal
	write8(BNO055_SYS_TRIGGER_ADDR, 0x80);
	HAL_Delay(1000);

	// set the operating mode to NDOF
	write8(BNO055_OPR_MODE_ADDR, OPERATION_MODE_NDOF);
	HAL_Delay(50);

	return true;
}

// get an IMU reading in Euler angles and put it into the public storage vars
void IMU_getEuler() {
	// make a buffer to put the raw data in, plus vars for slightly processed data
	uint8_t buffer[6] = {0,0,0,0,0,0};
	int16_t x, y, z;
	x = y = z = 0;

	// read the data (TODO multi-byte reads might not work how I expect)
	sendByte(BNO055_EULER_H_LSB_ADDR);
	HAL_I2C_Master_Receive(&hi2c1, BNO055_ADDR<<1, buffer, 6, 100);

	// break out the data into 3 numbers
	// 1 degree = 16 LSB (x in degrees is x/16.0)
	x = ((int16_t)buffer[0]) | (((int16_t)buffer[1]) << 8);
	y = ((int16_t)buffer[2]) | (((int16_t)buffer[3]) << 8);
	z = ((int16_t)buffer[4]) | (((int16_t)buffer[5]) << 8);

	// set the public vars
	IMU_pitchf = ((double)y)/16.0;
	IMU_rollf = ((double)z)/16.0;
	IMU_yawf = ((double)x)/16.0;

	IMU_pitch = (int)(IMU_pitchf*10000.0);
	IMU_roll = (int)(IMU_rollf*10000.0);
	IMU_yaw = (int)(IMU_yawf*10000.0);

}

// get an IMU reading as a Quaternion and put it into the public storage vars
void IMU_getQuat() {
	// make a buffer to put the raw data in, plus vars for slightly processed data
	uint8_t buffer[8] = {0,0,0,0,0,0,0,0};
	int16_t w, x, y, z;
	w = x = y = z = 0;

	// read the data (TODO multi-byte reads might not work how I expect)
	sendByte(BNO055_QUATERNION_DATA_W_LSB_ADDR);
	HAL_I2C_Master_Receive(&hi2c1, BNO055_ADDR<<1, buffer, 8, 100);

	// break out the data into 4 numbers
	w = (((uint16_t)buffer[1]) << 8) | ((uint16_t)buffer[0]);
	x = (((uint16_t)buffer[3]) << 8) | ((uint16_t)buffer[2]);
	y = (((uint16_t)buffer[5]) << 8) | ((uint16_t)buffer[4]);
	z = (((uint16_t)buffer[7]) << 8) | ((uint16_t)buffer[6]);

	// set the public vars
	const double scale = (1.0 / (1 << 14));
	IMU_qw = scale * w;
	IMU_qx = scale * x;
	IMU_qy = scale * y;
	IMU_qz = scale * z;
}


