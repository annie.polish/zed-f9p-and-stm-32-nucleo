/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

// controllerstech boilerplate ring buffer code
#include "UartRingbuffer.h"

// my code for parsing config files
#include "configParser.h"
#include "log.h"

#include "neopixel.h"

// my code for decoding UBX packets
#include "ubx.h"

#ifdef ROLE_BASE
#include "nmea.h"
#include "userinterface.h"
#endif

#ifdef ROLE_ROVER
#include "BNO055.h"
#endif

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

SPI_HandleTypeDef hspi3;
DMA_HandleTypeDef hdma_spi3_tx;

TIM_HandleTypeDef htim7;

UART_HandleTypeDef huart4;
UART_HandleTypeDef huart2;
UART_HandleTypeDef huart3;
DMA_HandleTypeDef hdma_uart4_rx;
DMA_HandleTypeDef hdma_usart2_rx;
DMA_HandleTypeDef hdma_usart2_tx;
DMA_HandleTypeDef hdma_usart3_tx;

/* USER CODE BEGIN PV */

bool Initialized = false; // has init task run?

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_UART4_Init(void);
static void MX_I2C1_Init(void);
static void MX_TIM7_Init(void);
static void MX_SPI3_Init(void);
static void MX_USART3_UART_Init(void);
/* USER CODE BEGIN PFP */
void findLogName();
#ifdef ROLE_ROVER
void roverInitTask ();
//void roverLoggerTask();
#endif
#ifdef ROLE_BASE
void baseInitTask ();
//void baseViewerTask();
//void lcdTask();
#endif

static void I2C_ClearBusyFlagErratum(I2C_HandleTypeDef* handle, uint32_t timeout);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART2_UART_Init();
  MX_UART4_Init();
  MX_I2C1_Init();
  MX_TIM7_Init();
  MX_SPI3_Init();
  MX_USART3_UART_Init();
  /* USER CODE BEGIN 2 */
	// check role definitions

	// 2 roles defined
#ifdef ROLE_ROVER
#ifdef ROLE_BASE
	exit(-1);
#endif
#endif
	// no roles defined
#ifndef ROLE_ROVER
#ifndef ROLE_BASE
	exit(-1);
#endif
#endif

	// run personal init functions
	Ringbuf_init();
	HAL_TIM_Base_Start(&htim7);

#ifdef ROLE_ROVER
	//openLogParseConfig();

	initLEDMOSI();
	LED_setColor(0,0,0);
	rainbowDelay(5000);

	I2C_ClearBusyFlagErratum(&hi2c1, 1000);
	IMU_begin();

	__HAL_UART_ENABLE_IT(&huart2, UART_IT_IDLE);

	openLogParseConfig();

	// with all other functions initialized, start up the UART interrupts
	//__HAL_UART_ENABLE_IT(&huart2, UART_IT_IDLE);

	sendRoverConfig();

	// FOR DEBUG
	//sendTPConfig(400000, 200000);

	// actually send the new config!!
	cfg_clear();
	//cfgRate = 1000;
	//cfgRate = 500;
	//cfgRate = 200;
	//cfgRate = 100;
	if (cfgRate >= 25 && cfgRate <= 10000) cfg_add(CFG_RATE_MEAS, &cfgRate, 2);
	cfg_transmit();

	sendTPConfig(cfgTPPeriod, cfgTPLen);
	sendTelemConfig(cfgTelem);
	//sendTelemConfig(false);

	// set LED to purple for waiting
	LED_setColor(255, 0, 200);
	LED_refreshTask();

	// FOR DEBUG
	//sendTPConfig(400000, 200000);

#endif


#ifdef ROLE_BASE
#ifndef MODE_EMERGENCY
//	HAL_Delay(500); // give the display half a second before trying to talk to it
//	DISP_init();
//	initLEDMOSI();
//	//lcd_clear();
//
//	setPixelColor(0, 0, 0, 0);
//	DISP_setBacklight(0,0,0);
//
//	HAL_Delay(1000);
//	setPixelColor(0, 100, 0, 255);
//	lcd_setbacklight(100,0,255);
//	// with all other functions initialized, start up the UART interrupts
//	__HAL_UART_ENABLE_IT(&huart2, UART_IT_IDLE);
//
//	// update the UI
//	DISP_warmup();
//
//	__HAL_UART_ENABLE_IT(&huart4, UART_IT_IDLE);
//
//	// send the initial config packets
//	//cfgTime = 60;
//	HAL_Delay(500);
//	sendBaseConfig();
//
//	findLogName();
//
//	// parse the config file
//	//parseConfig();
//	openLogParseConfig();
//	if (cfgSurvey) {
//		// start the survey by sending the packet
//		sendStartSurvey(cfgTime, cfgAcc);
//	} else {
//		// I hope the file has a location lock
//		// send the location lock packet
//		sendLockLocation(cfgLat, cfgLon, cfgHeight);
//	}

	// initialize my peripherals and give the display a second to boot up
	initLEDMOSI();
	rainbowDelay(3000);
	DISP_init();
#endif
	// turn on the UART interrupts
	__HAL_UART_ENABLE_IT(&huart2, UART_IT_IDLE);
	__HAL_UART_ENABLE_IT(&huart4, UART_IT_IDLE);

	// parse the config file and send the initial config packet
	openLogParseConfig();
	sendBaseConfig();

#ifndef MODE_EMERGENCY
	// wait for the GPS to see enough satellites
	DISP_warmup();
	uint32_t lastRefresh = HAL_GetTick();
	while(UBX_numSV < MIN_SATS) { // blocking on seeing enough satellites
		if (FLAG_UBX_READY) { UBXIdle(); } // read the packet and update the public vars
		if (HAL_GetTick() - lastRefresh > 1000) { // run the display at 1Hz
			DISP_warmup();
			lastRefresh = HAL_GetTick();
		}
	}

	// at this point we've seen enough satellites to start getting valid packets
	// create a log file
	// this part is still todo

	// if the config file requests a survey...
	if (cfgSurvey) {
		// todo log the survey specs
		// start the survey by sending the packet
		sendStartSurvey(cfgTime, cfgAcc);

		// block to make sure the next packets we get are actually during the survey
		while (!UBX_svinActive && !UBX_locked) {
			if (FLAG_UBX_READY) { UBXIdle(); }
//			lastRefresh = 0;
//			if (HAL_GetTick() - lastRefresh > 500) { // run the display at 2Hz
//				DISP_notLocked();
//				lastRefresh = HAL_GetTick();
//			}
		}

		// block until the survey is finished, updating the display and UBX vars
		while(UBX_svinActive) { // blocking on seeing enough satellites
			if (FLAG_UBX_READY) {
				UBXIdle(); // read the packet and update the public vars
				DISP_surveying(); // update the display whenever new data comes in
			}
			//if (HAL_GetTick() - lastRefresh > 500) { // run the display at 2Hz
			//	DISP_surveying();
			//	lastRefresh = HAL_GetTick();
			//}
		}
	// else, we've requested a location lock
	} else {
		// todo log the location lock

		// send the location lock packet
		sendLockLocation(cfgLat, cfgLon, cfgHeight);
	}

	// wait for one extra packet
	while(!FLAG_UBX_READY);
	UBXIdle();
	setPixelColor(0, 100, 70, 0);

	// block until the location is locked
	while (!UBX_locked) {
		if (FLAG_UBX_READY) { UBXIdle(); } // read the packet and update the public vars
		if (HAL_GetTick() - lastRefresh > 1000) { // run the display at 1Hz
			DISP_notLocked(); // display an error message
			lastRefresh = HAL_GetTick();

			// if we're here, the base hasn't locked yet.
			// if a manual lock was requested, keep resending that packet
			sendLockLocation(cfgLat, cfgLon, cfgHeight);
		}
	}

	// display the locked location for 5 seconds
	DISP_loc();
	//rainbowDelay(5000); // better delay and led behavior todo
	LED_setColor(255, 0, 100);
	LED_setMode(LED_MODE_FADE);
	// display a message about waiting to connect
	DISP_waitingRover();
	while(!FLAG_NMEA_READY) {
		LED_refreshTask();
	}
	LED_setMode(LED_MODE_SOLID);

	// end of base setup code
#endif // MODE_EMERGENCY
#endif //ROLE_BASE

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	uint32_t last_UBX_iTOW = UBX_iTOW;
	while (1)
	{
#ifdef ROLE_ROVER
		// the main loop for the rover
		//IMU_getEuler();

		// first, deal with any packet if there is one
		if (FLAG_UBX_READY) {
			UBXIdle();
			IMU_getEuler();
		}

		// log a packet if applicable
		//HAL_GPIO_TogglePin(GPIOC, DEB2_Pin);
		if (FLAG_UBX_PARSED && last_UBX_iTOW != UBX_iTOW) {
			last_UBX_iTOW = UBX_iTOW;
			roverLoggerTask();
		}
		//HAL_GPIO_TogglePin(GPIOC, DEB2_Pin);

		// refresh the LED (does very little, unless you're in blink or fade mode)
		LED_refreshTask();

		// check some of the variables set during UBX parsing to update the status LED
		// the order of these if statements determines the priority order status conditions
		// i.e., the dead man switch is highest priority

		// BLINKING RED: dead man switch: check if it's been a LONG time since the last packet
		//HAL_GPIO_TogglePin(GPIOC, DEB3_Pin);
		if (UBX_thisTick - UBX_lastTick > 5000) {
			LED_setColor(255, 0, 0);
			LED_setMode(LED_MODE_BLINK);
			LED_setRate(500);
		// TEST CODE!! DEACTIVATE BEFORE PRODUCTION!!
//		} else if (UBX_numSV != 0) {
//			LED_setColor(0, 255, 0);
//			LED_setMode(LED_MODE_BLINK);
//			LED_setRate(1000);
			// BLINKING ORANGE: Corrections not working properly
		} else if (UBX_diffStatus == 0) {
			LED_setColor(255, 100, 0);
			LED_setMode(LED_MODE_BLINK);
			LED_setRate(1000);
			// BLINKING YELLOW: Time not fully resolved
		} else if (UBX_fullyResolved == 0) {
			LED_setColor(255, 200, 0);
			LED_setMode(LED_MODE_BLINK);
			LED_setRate(1000);
			// FADING BLUE: Waiting for carrier phase fix
		} else if (UBX_carrierStatus != 2) {
			LED_setColor(0, 0, 255);
			LED_setMode(LED_MODE_FADE);
			LED_setRate(1000);
			// SOLID CYAN: happy and ready to fly
		} else {
			LED_setColor(0, 255, 255);
			LED_setMode(LED_MODE_SOLID);
		}
		//HAL_GPIO_TogglePin(GPIOC, DEB3_Pin);


#endif //ROLE_ROVER

#ifdef ROLE_BASE
		if (FLAG_UBX_READY) {
			//UBXIdle();
		}
		if (FLAG_NMEA_READY) {
			NMEAIdle();
			DISP_status();
		}


		if (UBX_svinActive) {
			// turn the LED a funny color
			setPixelColor(0, 100, 70, 0);
			DISP_setBacklight(100,70,0);
		}
		else if (!UBX_locked) {
			// turn the LED a different color
			setPixelColor(0, 255, 0, 0);
			DISP_setBacklight(255,0,0);
		}
		else if (HAL_GetTick() - NMEA_lastTick > 10000) {
			// dead man switch on NMEA packets back from the rover
			NMEA_thisTick = HAL_GetTick();
			DISP_status();
			HAL_Delay(100);
			setPixelColor(0, 230, 50, 0);
			DISP_setBacklight(230,50,0);
		}
		else if (NMEA_navStat == NMEA_NAVSTAT_G3) {
			setPixelColor(0, 0, 0, 255);
			DISP_setBacklight(255, 200, 220);
		}
		else if (NMEA_navStat == NMEA_NAVSTAT_D3) {
			// green for great connection!
			setPixelColor(0, 0, 255, 0);
			DISP_setBacklight(255, 200, 220);
		}
		else {
			// turn the LED off
			setPixelColor(0, 0, 0, 0);
			DISP_setBacklight(255, 200, 220);
		}

		//lcdTask();
		//if (HAL_GetTick() - lastRefresh > 200) {
		//	lastRefresh = HAL_GetTick();
		//	DISP_status();
		//}
#endif


    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL3;
  RCC_OscInitStruct.PLL.PLLDIV = RCC_PLL_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 20000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */
  I2C_ClearBusyFlagErratum(&hi2c1, 1000);

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief SPI3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI3_Init(void)
{

  /* USER CODE BEGIN SPI3_Init 0 */

  /* USER CODE END SPI3_Init 0 */

  /* USER CODE BEGIN SPI3_Init 1 */

  /* USER CODE END SPI3_Init 1 */
  /* SPI3 parameter configuration*/
  hspi3.Instance = SPI3;
  hspi3.Init.Mode = SPI_MODE_MASTER;
  hspi3.Init.Direction = SPI_DIRECTION_2LINES;
  hspi3.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi3.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi3.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi3.Init.NSS = SPI_NSS_SOFT;
  hspi3.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8;
  hspi3.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi3.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi3.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi3.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI3_Init 2 */

  /* USER CODE END SPI3_Init 2 */

}

/**
  * @brief TIM7 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM7_Init(void)
{

  /* USER CODE BEGIN TIM7_Init 0 */

  /* USER CODE END TIM7_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM7_Init 1 */

  /* USER CODE END TIM7_Init 1 */
  htim7.Instance = TIM7;
  htim7.Init.Prescaler = 32-1;
  htim7.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim7.Init.Period = 65535;
  htim7.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim7) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim7, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM7_Init 2 */

  /* USER CODE END TIM7_Init 2 */

}

/**
  * @brief UART4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_UART4_Init(void)
{

  /* USER CODE BEGIN UART4_Init 0 */

  /* USER CODE END UART4_Init 0 */

  /* USER CODE BEGIN UART4_Init 1 */

  /* USER CODE END UART4_Init 1 */
  huart4.Instance = UART4;
  huart4.Init.BaudRate = 115200;
  huart4.Init.WordLength = UART_WORDLENGTH_8B;
  huart4.Init.StopBits = UART_STOPBITS_1;
  huart4.Init.Parity = UART_PARITY_NONE;
  huart4.Init.Mode = UART_MODE_TX_RX;
  huart4.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart4.Init.OverSampling = UART_OVERSAMPLING_8;
  if (HAL_UART_Init(&huart4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN UART4_Init 2 */

  /* USER CODE END UART4_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART3_UART_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 57600;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_8;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */

  /* USER CODE END USART3_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel2_IRQn);
  /* DMA1_Channel6_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel6_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel6_IRQn);
  /* DMA1_Channel7_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel7_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel7_IRQn);
  /* DMA2_Channel2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Channel2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Channel2_IRQn);
  /* DMA2_Channel3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Channel3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Channel3_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, DEB0_Pin|DEB1_Pin|DEB2_Pin|DEB3_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(SD_CS_GPIO_Port, SD_CS_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : DEB0_Pin DEB1_Pin DEB2_Pin DEB3_Pin */
  GPIO_InitStruct.Pin = DEB0_Pin|DEB1_Pin|DEB2_Pin|DEB3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : LED1_Pin */
  GPIO_InitStruct.Pin = LED1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : SD_CS_Pin */
  GPIO_InitStruct.Pin = SD_CS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(SD_CS_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

// i2c busy flag clearing
// from https://electronics.stackexchange.com/a/398198
static bool wait_for_gpio_state_timeout(GPIO_TypeDef *port, uint16_t pin, GPIO_PinState state, uint32_t timeout)
 {
    uint32_t Tickstart = HAL_GetTick();
    bool ret = true;
    /* Wait until flag is set */
    for(;(state != HAL_GPIO_ReadPin(port, pin)) && (true == ret);)
    {
        /* Check for the timeout */
        if (timeout != HAL_MAX_DELAY)
        {
            if ((timeout == 0U) || ((HAL_GetTick() - Tickstart) > timeout))
            {
                ret = false;
            }
            else
            {
            }
        }
        asm("nop");
    }
    return ret;
}

#define I2C1_SCL_Pin GPIO_PIN_8
#define I2C1_SCL_GPIO_Port GPIOB
#define I2C1_SDA_Pin GPIO_PIN_9
#define I2C1_SDA_GPIO_Port GPIOB
static void I2C_ClearBusyFlagErratum(I2C_HandleTypeDef* handle, uint32_t timeout)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    // 1. Clear PE bit.
    CLEAR_BIT(handle->Instance->CR1, I2C_CR1_PE);

    //  2. Configure the SCL and SDA I/Os as General Purpose Output Open-Drain, High level (Write 1 to GPIOx_ODR).
    HAL_I2C_DeInit(handle);

    GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_OD;
    GPIO_InitStructure.Pull = GPIO_NOPULL;

    GPIO_InitStructure.Pin = I2C1_SCL_Pin;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);

    GPIO_InitStructure.Pin = I2C1_SDA_Pin;
    HAL_GPIO_Init(I2C1_SDA_GPIO_Port, &GPIO_InitStructure);

    // 3. Check SCL and SDA High level in GPIOx_IDR.
    HAL_GPIO_WritePin(I2C1_SDA_GPIO_Port, I2C1_SDA_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(I2C1_SCL_GPIO_Port, I2C1_SCL_Pin, GPIO_PIN_SET);

    wait_for_gpio_state_timeout(I2C1_SCL_GPIO_Port, I2C1_SCL_Pin, GPIO_PIN_SET, timeout);
    wait_for_gpio_state_timeout(I2C1_SDA_GPIO_Port, I2C1_SDA_Pin, GPIO_PIN_SET, timeout);

    // 4. Configure the SDA I/O as General Purpose Output Open-Drain, Low level (Write 0 to GPIOx_ODR).
    HAL_GPIO_WritePin(I2C1_SDA_GPIO_Port, I2C1_SDA_Pin, GPIO_PIN_RESET);

    // 5. Check SDA Low level in GPIOx_IDR.
    wait_for_gpio_state_timeout(I2C1_SDA_GPIO_Port, I2C1_SDA_Pin, GPIO_PIN_RESET, timeout);

    // 6. Configure the SCL I/O as General Purpose Output Open-Drain, Low level (Write 0 to GPIOx_ODR).
    HAL_GPIO_WritePin(I2C1_SCL_GPIO_Port, I2C1_SCL_Pin, GPIO_PIN_RESET);

    // 7. Check SCL Low level in GPIOx_IDR.
    wait_for_gpio_state_timeout(I2C1_SCL_GPIO_Port, I2C1_SCL_Pin, GPIO_PIN_RESET, timeout);

    // 8. Configure the SCL I/O as General Purpose Output Open-Drain, High level (Write 1 to GPIOx_ODR).
    HAL_GPIO_WritePin(I2C1_SCL_GPIO_Port, I2C1_SCL_Pin, GPIO_PIN_SET);

    // 9. Check SCL High level in GPIOx_IDR.
    wait_for_gpio_state_timeout(I2C1_SCL_GPIO_Port, I2C1_SCL_Pin, GPIO_PIN_SET, timeout);

    // 10. Configure the SDA I/O as General Purpose Output Open-Drain , High level (Write 1 to GPIOx_ODR).
    HAL_GPIO_WritePin(I2C1_SDA_GPIO_Port, I2C1_SDA_Pin, GPIO_PIN_SET);

    // 11. Check SDA High level in GPIOx_IDR.
    wait_for_gpio_state_timeout(I2C1_SDA_GPIO_Port, I2C1_SDA_Pin, GPIO_PIN_SET, timeout);

    // 12. Configure the SCL and SDA I/Os as Alternate function Open-Drain.
    GPIO_InitStructure.Mode = GPIO_MODE_AF_OD;
    GPIO_InitStructure.Alternate = GPIO_AF4_I2C1; //GPIO_AF1_I2C1;

    GPIO_InitStructure.Pin = I2C1_SCL_Pin;
    HAL_GPIO_Init(I2C1_SCL_GPIO_Port, &GPIO_InitStructure);

    GPIO_InitStructure.Pin = I2C1_SDA_Pin;
    HAL_GPIO_Init(I2C1_SDA_GPIO_Port, &GPIO_InitStructure);

    // 13. Set SWRST bit in I2Cx_CR1 register.
    SET_BIT(handle->Instance->CR1, I2C_CR1_SWRST);
    asm("nop");

    /* 14. Clear SWRST bit in I2Cx_CR1 register. */
    CLEAR_BIT(handle->Instance->CR1, I2C_CR1_SWRST);
    asm("nop");

    /* 15. Enable the I2C peripheral by setting the PE bit in I2Cx_CR1 register */
    SET_BIT(handle->Instance->CR1, I2C_CR1_PE);
    asm("nop");

    // Call initialization function.
    HAL_I2C_Init(handle);
}


// sets the name of the log file
// note that this will block until it receives a good UBX packet
// expect ~1s delay, but can block forever (if you never get a good fix)
void findLogName() {
	// wait for a packet with a good time lock
	bool madeLog = false;
	while (!madeLog) {
		if (FLAG_UBX_READY) UBXIdle();
		if (FLAG_UBX_PARSED) {//ubxHead != ubxTail) {
			UBXCheckPackets();
			if (UBXIsNextValid()) {
				// found a good packet, go make a log file and exit
				// find the config file name
				char filName[9];
				filName[8]=0;
//				uint8_t toRead = (ubxTail - 1)%BUF_SIZE;
//				if (ubxTail == 0) toRead = 0; // sanity check

				// build a UTC timestamp
				char utcStamp[32];

				sprintf(utcStamp, "%u-%02u-%02uT%02u:%02u:%02uZ\n", ubxIn.pvt.year,
						ubxIn.pvt.month, ubxIn.pvt.day, ubxIn.pvt.hour,
						ubxIn.pvt.min, ubxIn.pvt.sec);

				// pull a filename out of that timestamp
				filName[0] = utcStamp[5]; filName[1] = utcStamp[6]; // month
				filName[2] = utcStamp[8]; filName[3] = utcStamp[9]; // day
				filName[4] = utcStamp[11]; filName[5] = utcStamp[12]; // hour
				filName[6] = utcStamp[14]; filName[7] = utcStamp[15]; // minute

#ifdef ROLE_BASE
				filName[7] = 'L'; // minute
#endif

				// produce the log file and add a timestamp
				setLogName(filName);
				//writeLogLine(utcStamp);
				madeLog = true;
			}
			// increment both tails, because we don't do anything after parsing
			//ubxStrTail = (ubxStrTail + 1) % BUF_SIZE;
			//ubxTail = (ubxTail + 1) % BUF_SIZE;
		}
	}
}


#ifdef ROLE_ROVER
void roverInitTask () {
	// enable idle line interrupt
	__HAL_UART_ENABLE_IT(&huart2, UART_IT_IDLE);
	//__HAL_UART_ENABLE_IT(&huart4, UART_IT_IDLE);

	// send the initial config packet
	//TODO WAIT FOR GPS BOOTUP!!!!
	sendRoverConfig();

	// wait for a packet with a good time lock
	bool madeLog = false;
	while (!madeLog) {
		if (FLAG_UBX_PARSED) {//ubxHead != ubxTail) {
			UBXCheckPackets();
			if (UBXIsNextValid()) {
				// found a good packet, go make a log file and exit
				// find the config file name
				char filName[9];
				filName[8]=0;
//				uint8_t toRead = (ubxTail - 1)%BUF_SIZE;

				// build a UTC timestamp
				char utcStamp[32];

				int micros = ubxIn.pvt.nano / 1000;
				sprintf(utcStamp, "%u-%02u-%02uT%02u:%02u:%02u.%06uZ\n", ubxIn.pvt.year,
						ubxIn.pvt.month, ubxIn.pvt.day, ubxIn.pvt.hour,
						ubxIn.pvt.min, ubxIn.pvt.sec, micros);

				// pull a filename out of that timestamp
				filName[0] = utcStamp[5]; filName[1] = utcStamp[6]; // month
				filName[2] = utcStamp[8]; filName[3] = utcStamp[9]; // day
				filName[4] = utcStamp[11]; filName[5] = utcStamp[12]; // hour
				filName[6] = utcStamp[14]; filName[7] = 'L'; // minute

				// produce the log file and add a timestamp
				setLogName(filName);
				writeLogLine(utcStamp);
				madeLog = true;
			}
		}
	}

	// parse the config file
	parseConfig();

	// unsuspend the other tasks
	//vTaskResume(&roverLoggerHandler);
	Initialized = true;
}
#endif

//#ifdef ROLE_BASE
//void baseViewerTask() {
//	while (1) {
//		if (ubxHead != ubxTail) {
//			// run the checksum
//			UBXCheckPackets();
//			ubxTail = (ubxTail + 1) % BUF_SIZE;
//
//		}
//		HAL_Delay(100);
//	}
//}
//
//#endif

void vApplicationStackOverflowHook() {
	while (1) {

	}
}

/* USER CODE END 4 */

 /**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM6 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM6) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
