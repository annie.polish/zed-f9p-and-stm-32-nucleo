/*
 * serlcd.c
 *
 *  Created on: Dec 4, 2020
 *      Author: annie
 */

#include "serlcd.h"

I2C_HandleTypeDef hi2c1;


// from SparkFun SerLCD arduino library
#define LCD_ADDR 0x72 // i2c address of the LCD

//OpenLCD command characters
#define SPECIAL_COMMAND 254  //Magic number for sending a special command
#define SETTING_COMMAND 0x7C //124, |, the pipe character: The command to change settings: baud, lines, width, backlight, splash, etc

//OpenLCD commands
#define CLEAR_COMMAND 0x2D					//45, -, the dash character: command to clear and home the display
#define CONTRAST_COMMAND 0x18				//Command to change the contrast setting
#define ADDRESS_COMMAND 0x19				//Command to change the i2c address
#define SET_RGB_COMMAND 0x2B				//43, +, the plus character: command to set backlight RGB value
#define ENABLE_SYSTEM_MESSAGE_DISPLAY 0x2E  //46, ., command to enable system messages being displayed
#define DISABLE_SYSTEM_MESSAGE_DISPLAY 0x2F //47, /, command to disable system messages being displayed
#define ENABLE_SPLASH_DISPLAY 0x30			//48, 0, command to enable splash screen at power on
#define DISABLE_SPLASH_DISPLAY 0x31			//49, 1, command to disable splash screen at power on
#define SAVE_CURRENT_DISPLAY_AS_SPLASH 0x0A //10, Ctrl+j, command to save current text on display as splash

// special commands
#define LCD_RETURNHOME 0x02
#define LCD_ENTRYMODESET 0x04
#define LCD_DISPLAYCONTROL 0x08
#define LCD_CURSORSHIFT 0x10
#define LCD_SETDDRAMADDR 0x80

// flags for display entry mode
#define LCD_ENTRYRIGHT 0x00
#define LCD_ENTRYLEFT 0x02
#define LCD_ENTRYSHIFTINCREMENT 0x01
#define LCD_ENTRYSHIFTDECREMENT 0x00

// flags for display on/off control
#define LCD_DISPLAYON 0x04
#define LCD_DISPLAYOFF 0x00
#define LCD_CURSORON 0x02
#define LCD_CURSOROFF 0x00
#define LCD_BLINKON 0x01
#define LCD_BLINKOFF 0x00

// flags for display/cursor shift
#define LCD_DISPLAYMOVE 0x08
#define LCD_CURSORMOVE 0x00
#define LCD_MOVERIGHT 0x04
#define LCD_MOVELEFT 0x00

static uint8_t red, green, blue = 0;

uint8_t outbuf[32];

// write a byte to a given register
void send_setting(uint8_t val) {
	uint8_t datOut[2]; datOut[0] = SETTING_COMMAND; datOut[1] = val;
	HAL_I2C_Master_Transmit(&hi2c1, LCD_ADDR<<1, datOut, 2, 100);
}

// send a special command
void send_special(uint8_t val) {
	uint8_t datOut[2]; datOut[0] = SPECIAL_COMMAND; datOut[1] = val;
	HAL_I2C_Master_Transmit(&hi2c1, LCD_ADDR<<1, datOut, 2, 100);
}

// clear the display
void lcd_clear() {
	send_setting(CLEAR_COMMAND);
}

// print a string to the given line number
void lcd_print(char * lineOne, char * lineTwo) {
	// copy the strings into some local memory
	memcpy(outbuf, lineOne, 16);
	memcpy(outbuf+16, lineTwo, 16);
	send_setting(CLEAR_COMMAND);
	//send_special(LCD_RETURNHOME);
	HAL_Delay(1);
	HAL_I2C_Master_Transmit(&hi2c1, LCD_ADDR<<1, outbuf, 32, 100);
}

// set the RGB backlight
void lcd_setbacklight(uint8_t r, uint8_t g, uint8_t b) {
	if (r!=red || g!=green || b!=blue) {
		red = r; green = g; blue = b;
		send_setting(SET_RGB_COMMAND);
		HAL_I2C_Master_Transmit(&hi2c1, LCD_ADDR<<1, &r, 1, 100);
		HAL_I2C_Master_Transmit(&hi2c1, LCD_ADDR<<1, &g, 1, 100);
		HAL_I2C_Master_Transmit(&hi2c1, LCD_ADDR<<1, &b, 1, 100);
	}
}

// set the splash screen to the currently displayed text
void lcd_setsplash() {
	send_setting(ENABLE_SPLASH_DISPLAY);
	send_setting(SAVE_CURRENT_DISPLAY_AS_SPLASH);
}
