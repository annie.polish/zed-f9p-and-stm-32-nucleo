/*
 * userinterface.c
 *
 *  Created on: Jul 7, 2020
 *      Author: annie
 */

#include "userinterface.h"
#include <configParser.h>
#include "stm32l1xx_hal.h"
#include "serlcd.h"
#include "log.h"
#include <string.h>
#include <stdio.h>
#include "main.h"
#include "nmea.h"
#include "ubx.h"

uint8_t FLAG_LCD_REFRESH=0;  // the LCD should be refreshed now

// state variables and such
int loadPos = 0;
int loadLim = 5;
bool loadGoingUp = true;
int warmupCounter;

// for the state machine
#define UI_BOOTUP 0
#define UI_WARMUP 1
#define UI_READCFG 2
#define UI_SVRUNNING 3
#define UI_LOCKED 4
#define UI_ROVERPOS 5
#define UI_ROVERSTAT 6
#define UI_BASE 7
#define UI_ERRCFG 8
#define UI_ERRLOCK 9

// the screen buffer
char firstLine[17];
char secondLine[17];

// initialize the display and backlight
void DISP_init() {
	lcd_clear();
	lcd_setbacklight(255, 200, 220);
}

// a wrapper to access the serlcd lcd_setbacklight function
void DISP_setBacklight(uint8_t r, uint8_t g, uint8_t b) {
	lcd_setbacklight(r, g, b);
}

// displays the number of satellites currently connected, plus a loading animation
void DISP_warmup() {
	static int pos = 0; // position of the loading dot
	static bool goingUp = true; // which direction is the loading dot going?

	// run the loading animation on the first line
	strncpy(firstLine, "Warming up      ", 17);
	if (goingUp) {
		pos++;
		if (pos >= 5) goingUp = false;
	} else {
		pos--;
		if (pos <= 0) goingUp = true;
	}
	firstLine[10+pos] = '.';

	// track the satellites found so far on the second line
	strncpy(secondLine, "Found 00 Sats   ", 17);
	secondLine[6] = (char)(UBX_numSV/10 + 0x30);
	secondLine[7] = (char)(UBX_numSV%10 + 0x30);

	lcd_print(firstLine, secondLine);
}

// displays the current progress through the survey-in process
void DISP_surveying() {
	// updates the current value of the SVIN progress bar
	// needs data from the base buffer
	strncpy(firstLine, "SVIN is running!", 17);

	// print the accuracy in centimeters
	int seconds = UBX_svinProg % 60;
	int minutes = (UBX_svinProg - seconds)/60;
	sprintf(secondLine, "%6lucm \245%3d:%02d", UBX_svinAcc/100, minutes, seconds);

	lcd_print(firstLine, secondLine);
}

// displays the location of the base station
void DISP_loc() {
	// this function is a mess because this STM32 does NOT like floats,
	// and it especially hates printing floats onto strings
	int lat, lon, latWhole, lonWhole, latDec, lonDec, latSign, lonSign;

	// read in our own copy of lat and lon
	lat = UBX_lat; lon = UBX_lon;

	// keep track of the sign
	latSign = 1; lonSign = 1;
	if (UBX_lat < 0) { latSign = -1; lat = -1 * lat; }
	if (UBX_lon < 0) { latSign = -1; lon = -1 * lon; }

	// peel off the whole numbered degrees from each coordinate
	latWhole = (lat / 10000000);
	lonWhole = (lon / 10000000);

	// now grab the part after the decimal point
	latDec = lat - (latWhole * 10000000);
	lonDec = lon - (lonWhole * 10000000);

	// put the sign back in
	latWhole *= latSign;
	lonWhole *= lonSign;

	// finally ready to print
	sprintf(firstLine, "%4d.%07d Lat", latWhole, latDec);
	sprintf(secondLine, "%4d.%07d Lon", lonWhole, lonDec);

	lcd_print(firstLine, secondLine);
}

void DISP_tracker() {

}

// displays an error message about the base station not being locked
void DISP_notLocked() {
	strncpy(firstLine,  "NOT READY TO FLY", 17);
	strncpy(secondLine, "BASE NOT LOCKED!", 17);
	lcd_print(firstLine, secondLine);
}

// displays a message asking the user to turn on the rover
void DISP_waitingRover() {
	strncpy(firstLine,  "Base ready...   ", 17);
	strncpy(secondLine, "Power on rover  ", 17);
	lcd_print(firstLine, secondLine);
}

// the normal state of the display. shows information about the rover and its connection
void DISP_status() {
	static int dataRate;
	// check the NMEA dead man switch
	if (NMEA_thisTick - NMEA_lastTick > NMEA_TIMOUT) {
		strncpy(firstLine,  "ERR:NO TELEMETRY", 17);
		strncpy(secondLine, "                ", 17);
	} else {
		// receiving NMEA packets as expected
		// output the approximate data rate in Hz
		//strncpy(firstLine,  "Connected |   Hz", 17);
		dataRate = NMEA_thisTick - NMEA_lastTick;
		sprintf(firstLine, "Connected\245%4dms", dataRate);


		// check if the rover is actually doing differential corrections
		if (NMEA_navStat == NMEA_NAVSTAT_D3) {
			sprintf(secondLine, "RTK On \245%4lu.%lucm", NMEA_roverAcc/10, NMEA_roverAcc%10);
		} else {
			sprintf(secondLine, "NO RTK!\245%4lu.%lucm", NMEA_roverAcc/10, NMEA_roverAcc%10);
		}
	}
	lcd_print(firstLine, secondLine);
}

void DISP_setSplash() {
	strncpy(firstLine,  "uBlox RTK Logger", 17);
	strncpy(secondLine, "                ", 17);

	lcd_print(firstLine, secondLine);
	lcd_setsplash();
}
