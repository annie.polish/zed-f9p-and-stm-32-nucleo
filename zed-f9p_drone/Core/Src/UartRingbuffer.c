/*
 * UartRingbuffer.c
 *
 *  Created on: 10-Jul-2019
 *      Author: Controllerstech
 *
 *  Modified on: 11-April-2020
 */

#include "UartRingbuffer.h"
#include <string.h>
#include "ubx.h"
#include "nmea.h"

/**** define the UART you are using  ****/

extern UART_HandleTypeDef huart2;
extern UART_HandleTypeDef huart4;

#define local_uart &huart2
#define xbee_uart &huart4

/* put the following in the ISR 

extern void Uart_isr (UART_HandleTypeDef *huart);

 */

/****************=======================>>>>>>>>>>> NO CHANGES AFTER THIS =======================>>>>>>>>>>>**********************/

ring_buffer rx_buffer1 = { { 0 }, 0, 0};
ring_buffer tx_buffer1 = { { 0 }, 0, 0};
ring_buffer rx_buffer2 = { { 0 }, 0, 0};
ring_buffer tx_buffer2 = { { 0 }, 0, 0};

ring_buffer *_rx_buffer1;
ring_buffer *_tx_buffer1;
ring_buffer *_rx_buffer2;
ring_buffer *_tx_buffer2;

void store_char (unsigned char c, ring_buffer *buffer);


void Ringbuf_init(void)
{
	_rx_buffer1 = &rx_buffer1;
	_tx_buffer1 = &tx_buffer1;
	_rx_buffer2 = &rx_buffer2;
	_tx_buffer2 = &tx_buffer2;

	/* Enable the UART Error Interrupt: (Frame error, noise error, overrun error) */
	__HAL_UART_ENABLE_IT(local_uart, UART_IT_ERR);
	__HAL_UART_ENABLE_IT(xbee_uart, UART_IT_ERR);

	/* Enable the UART Data Register not empty Interrupt */
	__HAL_UART_ENABLE_IT(local_uart, UART_IT_RXNE);
	__HAL_UART_ENABLE_IT(xbee_uart, UART_IT_RXNE);
}

void store_char(unsigned char c, ring_buffer *buffer)
{
	int i = (unsigned int)(buffer->head + 1) % UART_BUFFER_SIZE;

	// if we should be storing the received character into the location
	// just before the tail (meaning that the head would advance to the
	// current location of the tail), we're about to overflow the buffer
	// and so we don't write the character or advance the head.
	if(i != buffer->tail) {
		buffer->buffer[buffer->head] = c;
		buffer->head = i;
	}
}

int Uart_read(UART_HandleTypeDef *uart)
{
	if (uart == local_uart)
	{
		// if the head isn't ahead of the tail, we don't have any characters
		if(_rx_buffer1->head == _rx_buffer1->tail)
		{
			return -1;
		}
		else
		{
			unsigned char c = _rx_buffer1->buffer[_rx_buffer1->tail];
			_rx_buffer1->tail = (unsigned int)(_rx_buffer1->tail + 1) % UART_BUFFER_SIZE;
			return c;
		}
	}

	else if (uart == xbee_uart)
	{
		// if the head isn't ahead of the tail, we don't have any characters
		if(_rx_buffer2->head == _rx_buffer2->tail)
		{
			return -1;
		}
		else
		{
			unsigned char c = _rx_buffer2->buffer[_rx_buffer2->tail];
			_rx_buffer2->tail = (unsigned int)(_rx_buffer2->tail + 1) % UART_BUFFER_SIZE;
			return c;
		}
	}

	else return -1;
}

void Uart_write(int c, UART_HandleTypeDef *uart)
{
	if (c>=0)
	{
		if (uart == local_uart){
			int i = (_tx_buffer1->head + 1) % UART_BUFFER_SIZE;

			// If the output buffer is full, there's nothing for it other than to
			// wait for the interrupt handler to empty it a bit
			// ???: return 0 here instead?
			while (i == _tx_buffer1->tail);

			_tx_buffer1->buffer[_tx_buffer1->head] = (uint8_t)c;
			_tx_buffer1->head = i;

			__HAL_UART_ENABLE_IT(local_uart, UART_IT_TXE); // Enable UART transmission interrupt
		}

		else if (uart == xbee_uart){
			int i = (_tx_buffer2->head + 1) % UART_BUFFER_SIZE;

			// If the output buffer is full, there's nothing for it other than to
			// wait for the interrupt handler to empty it a bit
			// ???: return 0 here instead?
			while (i == _tx_buffer2->tail);

			_tx_buffer2->buffer[_tx_buffer2->head] = (uint8_t)c;
			_tx_buffer2->head = i;

			__HAL_UART_ENABLE_IT(xbee_uart, UART_IT_TXE); // Enable UART transmission interrupt
		}
	}
}

int IsDataAvailable(UART_HandleTypeDef *uart)
{
	if (uart == local_uart) {
		return (uint16_t)(UART_BUFFER_SIZE + _rx_buffer1->head - _rx_buffer1->tail) % UART_BUFFER_SIZE;
	}
	else if (uart == xbee_uart) {
		return (uint16_t)(UART_BUFFER_SIZE + _rx_buffer2->head - _rx_buffer2->tail) % UART_BUFFER_SIZE;
	}
	return 0;
}

void Uart_isr (UART_HandleTypeDef *huart)
{
	uint32_t isrflags   = READ_REG(huart->Instance->SR);
	uint32_t cr1its     = READ_REG(huart->Instance->CR1);

	/* if DR is not empty and the Rx Int is enabled */
	if (((isrflags & USART_SR_RXNE) != RESET) && ((cr1its & USART_CR1_RXNEIE) != RESET))
	{
		/******************
		 *  @note   PE (Parity error), FE (Framing error), NE (Noise error), ORE (Overrun
		 *          error) and IDLE (Idle line detected) flags are cleared by software
		 *          sequence: a read operation to USART_SR register followed by a read
		 *          operation to USART_DR register.
		 * @note   RXNE flag can be also cleared by a read to the USART_DR register.
		 * @note   TC flag can be also cleared by software sequence: a read operation to
		 *          USART_SR register followed by a write operation to USART_DR register.
		 * @note   TXE flag is cleared only by a write to the USART_DR register.

		 *********************/
		huart->Instance->SR;                       /* Read status register */
		unsigned char c = huart->Instance->DR;     /* Read data register */
		if (huart == local_uart)
		{
			HAL_GPIO_TogglePin(DEB1_GPIO_Port, DEB1_Pin);
			store_char (c, _rx_buffer1);  // store data in buffer
		}

		else if (huart == xbee_uart)
		{
			store_char (c, _rx_buffer2);  // store data in buffer
		}

		return;
	}

	/*If interrupt is caused due to Transmit Data Register Empty */
	if (((isrflags & USART_SR_TXE) != RESET) && ((cr1its & USART_CR1_TXEIE) != RESET))
	{
		if (huart == local_uart){
			if(tx_buffer1.head == tx_buffer1.tail)
			{
				// Buffer empty, so disable interrupts
				__HAL_UART_DISABLE_IT(huart, UART_IT_TXE);

			}

			else
			{
				// There is more data in the output buffer. Send the next byte
				unsigned char c = tx_buffer1.buffer[tx_buffer1.tail];
				tx_buffer1.tail = (tx_buffer1.tail + 1) % UART_BUFFER_SIZE;

				/******************
				 *  @note   PE (Parity error), FE (Framing error), NE (Noise error), ORE (Overrun
				 *          error) and IDLE (Idle line detected) flags are cleared by software
				 *          sequence: a read operation to USART_SR register followed by a read
				 *          operation to USART_DR register.
				 * @note   RXNE flag can be also cleared by a read to the USART_DR register.
				 * @note   TC flag can be also cleared by software sequence: a read operation to
				 *          USART_SR register followed by a write operation to USART_DR register.
				 * @note   TXE flag is cleared only by a write to the USART_DR register.

				 *********************/

				huart->Instance->SR;
				huart->Instance->DR = c;

			}
		}

		else if (huart == xbee_uart){
			if(tx_buffer2.head == tx_buffer2.tail)
			{
				// Buffer empty, so disable interrupts
				__HAL_UART_DISABLE_IT(huart, UART_IT_TXE);

			}

			else
			{
				// There is more data in the output buffer. Send the next byte
				unsigned char c = tx_buffer2.buffer[tx_buffer2.tail];
				tx_buffer2.tail = (tx_buffer2.tail + 1) % UART_BUFFER_SIZE;

				/******************
				 *  @note   PE (Parity error), FE (Framing error), NE (Noise error), ORE (Overrun
				 *          error) and IDLE (Idle line detected) flags are cleared by software
				 *          sequence: a read operation to USART_SR register followed by a read
				 *          operation to USART_DR register.
				 * @note   RXNE flag can be also cleared by a read to the USART_DR register.
				 * @note   TC flag can be also cleared by software sequence: a read operation to
				 *          USART_SR register followed by a write operation to USART_DR register.
				 * @note   TXE flag is cleared only by a write to the USART_DR register.

				 *********************/

				huart->Instance->SR;
				huart->Instance->DR = c;

			}
		}
		return;
	}

	// If this is an idle line interrupt
	if (huart->Instance->SR & UART_FLAG_IDLE) {
		//HAL_GPIO_TogglePin(DEB3_GPIO_Port, DEB3_Pin);

		huart->Instance->SR;                       /* Read status register */
		huart->Instance->DR;                       /* Read data register */

		// call the correct UBX parser
#ifdef ROLE_ROVER
		FLAG_UBX_READY = 1;
		//UBXIdle(); //TODO THIS MIGHT NEED TO BE HERE
#endif

#ifdef ROLE_BASE
		if (huart == local_uart) {
			FLAG_UBX_READY = 1;
			//UBXIdle();
		}
		else if (huart == xbee_uart) {
			FLAG_NMEA_READY = 1;
			//NMEAIdle();
		}
#endif

	}
}
