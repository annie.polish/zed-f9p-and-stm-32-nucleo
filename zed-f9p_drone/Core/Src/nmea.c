/*
 * nmea.c
 *
 *  Created on: Oct 20, 2020
 *      Author: annie
 */

#include "nmea.h"
#include "UartRingbuffer.h"
#include "userinterface.h"
#include <string.h>
#include <stdio.h>
#include <stdbool.h>

uint8_t FLAG_NMEA_READY=0;   // an NMEA packet is waiting on the UART buffer

// public variables for accessing updated NMEA data
uint32_t NMEA_thisTick = 9999;
uint32_t NMEA_lastTick = 0;
uint32_t NMEA_roverAcc = 9999;
uint8_t NMEA_navStat = NMEA_NAVSTAT_NF;
int NMEA_lat; // current latitude in deg * 10^-7
int NMEA_lon; // current longitude in deg * 10^-7

// a local buffer for reading in a whole NMEA string at a time
// makes it easier to do checksums and stuff
#define MAX_NMEA_LEN 84
unsigned char nmeaIn[MAX_NMEA_LEN]; // the maximum length of an NMEA string is 82 characters

// the idle line ISR function
void NMEAIdle(int roverOrBase) {
	// wait for a $
	nmeaIn[0] = 0;
	while(IsDataAvailable(&huart4) && nmeaIn[0] != '$') {
		nmeaIn[0] = Uart_read(&huart4);
	}

	// read the whole packet
	nmeaIn[1] = Uart_read(&huart4); // fixes the top of while loop checking
	uint8_t idx = 2;
	while(!(nmeaIn[idx-2] == 0x0D && nmeaIn[idx-1] == 0x0A) && IsDataAvailable(&huart4) && idx < MAX_NMEA_LEN) {
		nmeaIn[idx] = Uart_read(&huart4);
		idx++;
	}

	// parse a PUBX00 packet
	idx = 0;
	while(nmeaIn[idx] == '$') idx++;
	if (nmeaIn[idx] == 'P' && nmeaIn[idx+1] == 'U' && nmeaIn[idx+2] == 'B' && nmeaIn[idx+6] == '0') {
		// update the timers
		NMEA_lastTick = NMEA_thisTick;
		NMEA_thisTick = HAL_GetTick();

		idx+=45; // skip time, lat, and lon

		// find the end of the altRef section
		while(nmeaIn[idx] != '.') idx++;
		while(nmeaIn[idx] != ',') idx++;
		idx++;

		// nav status
		if (nmeaIn[idx] == 'N' && nmeaIn[idx+1] == 'F') NMEA_navStat = NMEA_NAVSTAT_NF;
		else if (nmeaIn[idx] == 'D' && nmeaIn[idx+1] == 'R') NMEA_navStat = NMEA_NAVSTAT_DR;
		else if (nmeaIn[idx] == 'G' && nmeaIn[idx+1] == '2') NMEA_navStat = NMEA_NAVSTAT_G2;
		else if (nmeaIn[idx] == 'G' && nmeaIn[idx+1] == '3') NMEA_navStat = NMEA_NAVSTAT_G3;
		else if (nmeaIn[idx] == 'D' && nmeaIn[idx+1] == '2') NMEA_navStat = NMEA_NAVSTAT_D2;
		else if (nmeaIn[idx] == 'D' && nmeaIn[idx+1] == '3') NMEA_navStat = NMEA_NAVSTAT_D3;
		else if (nmeaIn[idx] == 'R' && nmeaIn[idx+1] == 'K') NMEA_navStat = NMEA_NAVSTAT_RK;
		else if (nmeaIn[idx] == 'T' && nmeaIn[idx+1] == 'T') NMEA_navStat = NMEA_NAVSTAT_TT;
		else NMEA_navStat = NMEA_NAVSTAT_NF;

		idx+=3;

		// hacc in cm
		NMEA_roverAcc = 0;
		while(nmeaIn[idx] != '.' && NMEA_roverAcc < 100) {
			NMEA_roverAcc *= 10;
			NMEA_roverAcc += (nmeaIn[idx] - 0x30);
			idx++;
		} idx++;
		NMEA_roverAcc *= 100;
		int multiplier = 10;
		while(nmeaIn[idx] != ',') {
			NMEA_roverAcc += (nmeaIn[idx] - 0x30) * multiplier;
			idx++; multiplier = multiplier/10;
		}

	}
	FLAG_NMEA_READY = 0;
}


