/*
 * nmea.h
 *
 *  Created on: Oct 20, 2020
 *      Author: annie
 */

#ifndef INC_NMEA_H_
#define INC_NMEA_H_

#include <stdint.h>
#include <stdbool.h>
#include <main.h>

// uart handle for use with UartRingBuffer
extern UART_HandleTypeDef huart4;

// flag to signal when a new NMEA packet is ready
extern uint8_t FLAG_NMEA_READY;

// public variables for accessing updated NMEA data
extern uint32_t NMEA_thisTick; // time of this packet on the internal tick clock
extern uint32_t NMEA_lastTick; // time of last packet on internal tick clock
extern uint32_t NMEA_roverAcc; // rover position accuracy in centimeters
extern uint8_t NMEA_navStat;   // rover navigation status (using the nav status symbols above)

extern int NMEA_lat; // current latitude in deg * 10^-7
extern int NMEA_lon; // current longitude in deg * 10^-7

// nav status symbols
#define NMEA_NAVSTAT_NF 0 // nofix
#define NMEA_NAVSTAT_DR 1 // dead reckoning
#define NMEA_NAVSTAT_G2 2 // standalone 2D
#define NMEA_NAVSTAT_G3 3 // standalone 3D
#define NMEA_NAVSTAT_D2 4 // diffcorrected 2D
#define NMEA_NAVSTAT_D3 5 // diffcorrected 3D
#define NMEA_NAVSTAT_RK 6 // GPS and dead reckoning
#define NMEA_NAVSTAT_TT 7 // time only fix

// the idle line ISR function
void NMEAIdle();

#endif /* INC_NMEA_H_ */
