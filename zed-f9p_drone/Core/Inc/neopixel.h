/*
 * neopixel.h
 *
 *  Created on: Jul 7, 2020
 *      Author: annie
 *
 *  WARNING!!
 *  This driver REQUIRES a SPI baud rate of exactly 3Mbits/s
 *  which requires a peripheral clock speed that is a multiple of 3MHz
 *
 *  If the LEDs don't work, check your clock configuration.
 *
 */

#ifndef INC_NEOPIXEL_H_
#define INC_NEOPIXEL_H_

#include "stm32l1xx_hal.h"

void initLEDMOSI();
void setPixelColor(uint16_t n, uint8_t r, uint8_t g, uint8_t b);
void setAllPixelColor(uint8_t r, uint8_t g, uint8_t b);
void setPixelHSV(uint16_t n, uint16_t hue, uint8_t sat, uint8_t val);
void rainbowDelay(uint32_t millis);

// set the LED color
void LED_setColor(uint8_t r, uint8_t g, uint8_t b);
// change the mode
void LED_setMode(uint8_t modeIn);
// change the rate
void LED_setRate(uint32_t rate) ;
// the main refresh task
void LED_refreshTask();

// defines the number of LEDs in the string
#define LED_NO    1

// defines the size of the DMA output buffer
#define LED_BUFFER_LENGTH (LED_NO*12)

// LED mode definitions
#define LED_MODE_SOLID 0
#define LED_MODE_BLINK 1
#define LED_MODE_FADE  2

#endif /* INC_NEOPIXEL_H_ */
