/*
 * configurator.h
 *
 *  Created on: Aug 26, 2020
 *      Author: annie
 */

#ifndef INC_CONFIGPARSER_H_
#define INC_CONFIGPARSER_H_

#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "main.h"

// has a config file been parsed yet?
extern bool cfgDone;

// was there an error in the config file?
extern bool cfgError;

// ACTUAL CONFIG VARIABLES
extern bool cfgSurvey; // is SURVEY set to true or false?
extern int32_t cfgLat; // fixed latitude, formatted to be ready to send
extern int32_t cfgLon;
extern int32_t cfgHeight;
extern uint32_t cfgTime;
extern uint32_t cfgAcc;
extern uint16_t cfgRate;
extern uint32_t cfgTPPeriod;
extern uint32_t cfgTPLen;
extern bool cfgTelem;


// parses a config file from the SD card
// you MUST run setLogName() first!!
//bool parseConfig();

bool openLogParseConfig();


#endif /* INC_CONFIGPARSER_H_ */
