/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define DEB0_Pin GPIO_PIN_0
#define DEB0_GPIO_Port GPIOC
#define DEB1_Pin GPIO_PIN_1
#define DEB1_GPIO_Port GPIOC
#define DEB2_Pin GPIO_PIN_2
#define DEB2_GPIO_Port GPIOC
#define DEB3_Pin GPIO_PIN_3
#define DEB3_GPIO_Port GPIOC
#define LED1_Pin GPIO_PIN_8
#define LED1_GPIO_Port GPIOA
#define SD_CS_Pin GPIO_PIN_6
#define SD_CS_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

// Maximum number of lines to include in a single log file
#define MAX_FILE_LEN 10000

// Number of lines to store in memory before writing to the SD card
#define DATA_BUF_SIZE 5

// Percentage brightness for the status LED
#define STATUS_BRIGHTNESS 50

// time to spend warming up at bootup
#define WARMUP_TIME 10

// threshold for the "good fix" status LED color
// in units of 0.1mm
#define FIX_THRESHOLD 1000

// how long to wait between packets from the rover to the base
// before triggering the dead man switch
// make sure this is longer than your slowest expected rover data rate
#define NMEA_TIMOUT 2000

// minimum number of satellites to start surveying
#define MIN_SATS 5

// Define either ROLE_BASE or ROLE_ROVER
//#define ROLE_BASE
#define ROLE_ROVER

//#define MODE_EMERGENCY


/* ------------- END OF USER SETTINGS ---------------------*/
/*Rover vs Base definitions*/
#define ROVER 1
#define BASE 2

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
