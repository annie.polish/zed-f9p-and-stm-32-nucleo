/*
 * serlcd.h
 *
 *  Created on: Dec 4, 2020
 *      Author: annie
 *
 *  Driver for the SparkFun SerLCD module, connected over Qwiic
 */

#ifndef INC_SERLCD_H_
#define INC_SERLCD_H_

#include <ctype.h>
#include <stdint.h>
#include <string.h>
#include "stm32l1xx_hal.h"
#include "main.h"

void lcd_clear();

void lcd_print(char * lineOne, char * lineTwo);

void lcd_setbacklight(uint8_t r, uint8_t g, uint8_t b);

void lcd_setsplash();

#endif /* INC_SERLCD_H_ */
