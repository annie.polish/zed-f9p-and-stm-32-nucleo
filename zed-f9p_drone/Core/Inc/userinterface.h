/*
 * userinterface.h
 *
 *  Created on: Jul 7, 2020
 *      Author: annie
 *
 *  Display function for putting data on the Qwiic LCD
 *
 */

#ifndef INC_USERINTERFACE_H_
#define INC_USERINTERFACE_H_

//#include "main.h"
#include <stdbool.h>
#include <stdint.h>

extern uint8_t FLAG_LCD_REFRESH;  // the LCD should be refreshed now

// public functions
void DISP_init();
void DISP_setBacklight(uint8_t r, uint8_t g, uint8_t b);
void DISP_warmup();
void DISP_surveying();
void DISP_loc();
void DISP_notLocked();
void DISP_waitingRover();
void DISP_status();
void DISP_setSplash();
void DISP_tracker();

#endif /* INC_USERINTERFACE_H_ */
