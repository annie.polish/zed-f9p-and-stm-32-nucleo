# ZED-F9P and STM 32 Nucleo

Code to run on an STM32 Nucleo board in order to support a uBlox ZED-F9P precision GNSS module.


The code is (hopefully) clearly commented. 
Check the `main.h` file for each project for flags and settings.


Written by Annie Polish, 2019-2022

# Hardware targets
`nucleo32_drone` requires a Nucleo32 breakout board with a STM32L432KC processor.

`zed-f9p_drone` requires a Nucleo64 breakout board with a STM32L152RE processor.

# Required software
This code was developed using STM32CubeIDE version 1.5.1.
You will definitely need to use some version of STM32CubeIDE (available for free from STM32), and you may need to go find an older version.

Note that STM32CubeIDE handles renaming files and folders astonishingly poorly - don't try to change the name of a project, instead copy each file that you want into a new project.

